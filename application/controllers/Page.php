<?php
class Page extends CI_Controller {

        public function detail($slug)
        {
        	$berita = $this->db->query("SELECT * FROM tik_news ORDER BY id DESC");	
	        $kategori = $this->db->query("SELECT * FROM tik_news_category");
	        $popular = $this->db->query("SELECT * FROM tik_news ORDER BY viewer DESC LIMIT 5");
	        $terbaru = $this->db->query("SELECT * FROM tik_news ORDER BY id DESC LIMIT 5");	

	        
	        $page = $this->db->query('SELECT * from tik_pages WHERE slug_url = ? LIMIT 1', array($slug));
	        $page = $page->row();

	        // Icrease viewer
	        $data_update = array('viewer' => $page->viewer + 1);
	        $this->db->where('slug_url', $slug);
	        $this->db->update('tik_pages', $data_update);

            $this->load->view('page', array('kategori' => $kategori, 'berita' => $berita, 'popular' => $popular, 'terbaru' => $terbaru, 'page' => $page ));


        }
}