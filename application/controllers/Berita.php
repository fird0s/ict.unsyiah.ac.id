<?php
class Berita extends CI_Controller {


    public function page($id = NULL){

        // berita hanya muncul apabila status 1 (Publish)
        $kategori = $this->db->query("SELECT * FROM tik_news_category");
        $popular = $this->db->query("SELECT * FROM tik_news ORDER BY viewer DESC LIMIT 5");
        $terbaru = $this->db->query("SELECT * FROM tik_news ORDER BY id DESC LIMIT 5"); 

        // pagination
        $this->load->library("pagination");
        $config["base_url"] = site_url("berita/index");
        $config["per_page"] = 4;
        $config["num_links"] = 10;
        $config["total_rows"] = $this->db->get("tik_news")->num_rows();

        // style css costum
        $config['full_tag_open'] = "<ul class='pagination pagination-lg pull-right'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        $this->db->order_by("id", "desc");
        $data["query"] = $this->db->get("tik_news", $config["per_page"], $this->uri->segment(3));


        $this->load->view('/berita/berita', array('data' => $data, 'kategori' => $kategori, 'popular' => $popular, 'terbaru' => $terbaru ));
    }



    public function index($id = NULL){

        // berita hanya muncul apabila status 1 (Publish)
        $kategori = $this->db->query("SELECT * FROM tik_news_category");
        $popular = $this->db->query("SELECT * FROM tik_news ORDER BY viewer DESC LIMIT 5");
        $terbaru = $this->db->query("SELECT * FROM tik_news ORDER BY id DESC LIMIT 5");	

        // pagination
        $this->load->library("pagination");
        $config["base_url"] = site_url("berita/index");
        $config["per_page"] = 4;
        $config["num_links"] = 10;
        $config["total_rows"] = $this->db->get("tik_news")->num_rows();

        // style css costum
        $config['full_tag_open'] = "<ul class='pagination pagination-lg pull-right'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        $this->db->order_by("id", "desc");
        $data["query"] = $this->db->get("tik_news", $config["per_page"], $this->uri->segment(3));


		$this->load->view('/berita/berita', array('data' => $data, 'kategori' => $kategori, 'popular' => $popular, 'terbaru' => $terbaru ));
	}

	public function detail($slug){
        
		$terbaru = $this->db->query("SELECT * FROM tik_news ORDER BY id DESC LIMIT 5");	
        $kategori = $this->db->query("SELECT * FROM tik_news_category");
        $popular = $this->db->query("SELECT * FROM tik_news ORDER BY viewer DESC LIMIT 5");
        $post = $this->db->query('SELECT * from tik_news WHERE slug = ? LIMIT 1', array($slug));
        $post = $post->row();

        // Icrease viewer
        $data_update = array('viewer' => $post->viewer + 1);
        $this->db->where('slug', $slug);
        $this->db->update('tik_news', $data_update);

		$this->load->view('/berita/detail', array('kategori' => $kategori, 'terbaru' => $terbaru, 'popular' => $popular, 'post' => $post ));
	}	

    public function kategori($slug){
        $kategori = $this->db->query("SELECT * FROM tik_news_category");
        $popular = $this->db->query("SELECT * FROM tik_news ORDER BY viewer DESC LIMIT 5");
        $terbaru = $this->db->query("SELECT * FROM tik_news ORDER BY id DESC LIMIT 5"); 


        $kategori_post = $this->db->query('SELECT * from tik_news_category WHERE slug = ?', array($slug));
        if (!$kategori_post->num_rows()){
            redirect('/berita', 'refresh');
        }

        // pagination

        $this->load->library("pagination");
        $config["base_url"] = site_url("berita/kategori/".$slug);
        $config["per_page"] = 4;
        $config["num_links"] = 10;
        
        $this->db->where('kategori', $kategori_post->row()->category_name);
        $this->db->order_by("id", "desc");
        $config["total_rows"] = $this->db->get("tik_news")->num_rows();

        // style css costum
        $config['full_tag_open'] = "<ul class='pagination pagination-lg pull-right'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);


        $this->db->where('kategori', $kategori_post->row()->category_name);
        $this->db->order_by("id", "desc");
        $data["query"] = $this->db->get("tik_news", $config["per_page"], $this->uri->segment(3));

        $this->load->view('/kategori/kategori', array('kategori_post' => $kategori_post, 'data' => $data, 'kategori' => $kategori, 'kategori' => $kategori, 'popular' => $popular, 'terbaru' => $terbaru));

    }
}