<?php


class Strukturorganisasi extends CI_Controller {

        public function index()
        {
            $devision = $this->db->query("SELECT * FROM tik_devision_category  ORDER BY sort ");
            $employee = $this->db->query("SELECT * FROM tik_devision_member ORDER BY sort ");
            $this->load->view('struktur-organisasi/struktur-organisasi', 
                array('devision' => $devision, 'employee' => $employee));
        }


        public function devision($slug)
        {
            $devision = $this->db->query("SELECT * FROM tik_devision_category ORDER BY sort ");    
            $get_devision = $this->db->query('SELECT * from tik_devision_category WHERE slug = ? LIMIT 1', array($slug));
            $employee = $this->db->query('SELECT * from tik_devision_member WHERE devision = ? ORDER BY sort', array($get_devision->row()->devision) );
            $this->load->view('struktur-organisasi/per_devision', 
                array('devision' => $devision, 'employee' => $employee, 'get_devision' => $get_devision));
        }


        // public function detail($slug_url){
        
        // $service = $this->db->query('SELECT * from tik_facilities_and_services WHERE slug_url = ? LIMIT 1', array($slug_url));
        // $service = $service->row();
        

        // // Icrease viewer
        // $data_update = array('viewer' => $service->viewer + 1);
        // $this->db->where('slug_url', $slug_url);
        // $this->db->update('tik_facilities_and_services', $data_update);

        // $this->load->view('fasilitan-dan-layanan/detail-fasilitas-dan-layanan', array('service' => $service ));
    // }   
}