<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$berita_terbaru = $this->db->query("SELECT * FROM tik_news WHERE status = 1 AND kategori LIKE 'Berita Terbaru' ORDER BY id DESC LIMIT 6");
		$pengunguman = $this->db->query("SELECT * FROM tik_news WHERE status = 1 AND kategori LIKE 'Pengumuman' ORDER BY id DESC LIMIT 6");
		$slide = $this->db->query("SELECT * FROM tik_slide ORDER BY sort");
		$testimonial = $this->db->query("SELECT * FROM tik_testimonial ORDER BY sort");
		$this->load->view('index', array('berita_terbaru' => $berita_terbaru, 'pengunguman' => $pengunguman, 'slide' => $slide, 'testimonial' => $testimonial));
	}

	

}
