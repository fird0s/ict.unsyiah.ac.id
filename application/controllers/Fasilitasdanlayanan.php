<?php

class Fasilitasdanlayanan extends CI_Controller {

        public function index()
        {
    		$facilities = $this->db->query("SELECT * FROM tik_facilities_and_services ORDER BY sort");
            $this->load->view('fasilitan-dan-layanan/fasilitas-dan-layanan', array(
            	'facilities' => $facilities
            ));
        }

        public function detail($slug_url){
        
        $service = $this->db->query('SELECT * from tik_facilities_and_services WHERE slug_url = ? LIMIT 1', array($slug_url));
        $service = $service->row();
        

        // Icrease viewer
        $data_update = array('viewer' => $service->viewer + 1);
        $this->db->where('slug_url', $slug_url);
        $this->db->update('tik_facilities_and_services', $data_update);

        $this->db->order_by('id', 'RANDOM');
        $this->db->limit(4);
        $random_post = $this->db->get('tik_facilities_and_services');

		$this->load->view('fasilitan-dan-layanan/detail-fasilitas-dan-layanan', array('service' => $service, 'random_post' => $random_post ));
	}	
}