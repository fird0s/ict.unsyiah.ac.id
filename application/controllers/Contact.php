<?php
class Contact extends CI_Controller {

        public function index()
        {


				if (isset($_POST['submit'])){
					if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['message']) ){

		        		$this->load->library('email');

						$this->email->from($_POST['email'], $_POST['name']);
						$this->email->to('helpdesk.ict@unsyiah.ac.id'); 

						$this->email->subject($_POST['subject']);
						$this->email->message($_POST['message']);	

						$this->email->send();
						$this->session->set_flashdata('success_msg', 'Pesan Anda sudah terkirim.');
						redirect('/contact', 'refresh');
					}else {
						$this->session->set_flashdata('error_msg', 'Terjadi kesalahan ketika mengirim pesan.');
					}
				}


                $this->load->view('contact');
        }
}