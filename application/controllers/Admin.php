<?php
class Admin extends CI_Controller {

		
		public function compress_image($source_url, $destination_url, $quality) {
			$info = getimagesize($source_url);
		 
			if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
			elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
			elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
		 
			//save file
			imagejpeg($image, $destination_url, $quality);
		 
			//return destination file
			return $destination_url;
		}


		function auth()
		{
			if($this->session->userdata('logged_in'))
			{
			    $session_data = $this->session->userdata('logged_in');
			    $data['email'] = $session_data['email'];
		    }else{
			    return redirect('/admin/login123', 'refresh');
		    }
		}

		public function index()
		{
			return redirect('/admin/login123', 'refresh');
		}

		public function add_new_user()
		{
			$this->load->library('encrypt');
			
			if (isset($_POST['submit'])){
				$new_user = array(
					'name' => $_POST['nama'],
					'email' => $_POST['email'],
					'no_hp' => $_POST['hp'],
					'password' => $this->encrypt->encode($_POST['pass']),
				);
				$res = $this->db->insert('tik_users', $new_user);
				if ($res) {
					$this->session->set_flashdata('success_msg', 'User baru berhasil ditambah.');
					return redirect('/admin/add_new_user', 'refresh');
				} elseif(!$res) {
					$this->session->set_flashdata('err_msg', 'Terjadi kesalahan.');
					return redirect('/admin/add_new_user', 'refresh');
				}
				   
			}

			$this->load->view("/admin/user/add_new_user");
		}

		public function user_profile()
		{

			$this->load->library('encrypt');	
			$this->auth();
			$session_data = $this->session->userdata('logged_in');

			$sql = 'SELECT * FROM tik_users WHERE email = ? LIMIT 1';
			$get_user = $this->db->query($sql, array($session_data['email']));
			$get_user = $get_user->row();

			if (isset($_POST['submit']))
			{
				if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['phone']) )
				{

					$data_update = array(
						'name' => $_POST['name'],
						'email' => $_POST['email'],
						'no_hp' => $_POST['phone']
					);



					$sess_array = array(
				       'email' => $data_update['email']
				    );

					$this->db->where("email", $get_user->email);
					$update = $this->db->update('tik_users', $data_update);

					if ($update){
						$this->session->unset_userdata('logged_in');
						$this->session->set_userdata('logged_in', $sess_array);
						$this->session->set_flashdata('success_msg', 'Mengubah data user.');
						return redirect('/admin/user_profile', 'refresh');
					}else{
						$this->session->set_flashdata('err_msg', 'Ada terjadi kesalahan.');
						return redirect('/admin/user_profile', 'refresh');
					}
				}	
			}	

			if (isset($_POST['change_pwd']))
			{

				if (isset($_POST['old_pass']) && isset($_POST['new_pass']) && isset($_POST['new_pass_verify']) ){

					if ($this->encrypt->decode($get_user->password) == $_POST['old_pass']){
						$data_update = array('password' => $this->encrypt->encode($_POST['new_pass']));
						$this->db->where("email", $get_user->email);
						$update = $this->db->update('tik_users', $data_update);
						$this->session->set_flashdata('success_msg', 'Password berhasil diubah.');
						return redirect('/admin/user_profile', 'refresh');
					}else{
						$this->session->set_flashdata('err_msg', 'Terjadi kesalahan pada perubahan password.');
						return redirect('/admin/user_profile', 'refresh');
					}
				}	

			}

			$this->load->view("/admin/user/user_profile", array('get_user' => $get_user));
		}

		public function logout()
		{
			$this->auth();	
			$this->session->unset_userdata('logged_in');
			session_destroy();
			return redirect('/admin/login123', 'refresh');
		}

		public function login123()

		{

		$this->load->library('encrypt');	

		if($this->session->userdata('logged_in'))
		   {
		     $session_data = $this->session->userdata('logged_in');
		     $data['email'] = $session_data['email'];
		     return redirect('/admin/dashboard', 'refresh');
		   }

		   
			if (isset($_POST['submit']))
			{
		       $user = $this->db->query('SELECT * from tik_users WHERE email = ? LIMIT 1', array($_POST['email']));
		       $user = $user->row();

		       if ($user){
		       		if ($this->encrypt->decode($user->password) == $_POST['password']){
		       			$sess_array = array('id' => $user->id, 'email' => $user->email);
		       			$this->session->set_userdata('logged_in', $sess_array);
					 	return redirect('/admin/dashboard', 'refresh');
		       		} else{
		       			$this->session->set_flashdata('err_msg', 'Email atau password salah.');
		       			return redirect('/admin/login123', 'refresh');
		       		}
		       }else{
		       		$this->session->set_flashdata('err_msg', 'Email atau password salah.');
	       			return redirect('/admin/login123', 'refresh');
		       }
			}	


			$this->load->view('admin/login');


		}

        public function dashboard()
        {
        	$this->auth();
        	$this->db->order_by("id", "desc"); 
        	$berita = $this->db->get_where('tik_news', array('status' => 1), 5);

        	$slide = $this->db->query("SELECT * FROM tik_slide ORDER BY sort LIMIT 5");
        	$pages = $this->db->query("SELECT * FROM tik_pages ORDER BY id DESC LIMIT 5");
        	$testimonial = $this->db->query("SELECT * FROM tik_testimonial ORDER BY sort DESC LIMIT 5");
            $this->load->view('admin/dashboard', array('berita' => $berita, 'slide' => $slide, 'pages' => $pages, 'testimonial' => $testimonial ));
        }

        public function berita($id = NULL)
        {
        		$this->auth();

        		// pagination
        		$this->load->library("pagination");
        		$config["base_url"] = site_url("admin/berita");
        		$config["per_page"] = 5;
        		$config["num_links"] = 10;
        		$config["total_rows"] = $this->db->get("tik_news")->num_rows();

        		// style css costum
        		$config['full_tag_open'] = "<ul class='pagination'>";
				$config['full_tag_close'] ="</ul>";
				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';
				$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
				$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
				$config['next_tag_open'] = "<li>";
				$config['next_tagl_close'] = "</li>";
				$config['prev_tag_open'] = "<li>";
				$config['prev_tagl_close'] = "</li>";
				$config['first_tag_open'] = "<li>";
				$config['first_tagl_close'] = "</li>";
				$config['last_tag_open'] = "<li>";
				$config['last_tagl_close'] = "</li>";

        		$this->pagination->initialize($config);

        		$this->db->order_by("id", "desc");
        		$data["query"] = $this->db->get("tik_news", $config["per_page"], $this->uri->segment(3));

                $this->load->view('admin/berita/berita', $data);
        }

        public function tambah_berita()
        {
        		$this->auth();
        		$kategori = $this->db->query("SELECT * FROM tik_news_category");

        		$session_data = $this->session->userdata('logged_in');
        		$user = $this->db->query('SELECT * from tik_users WHERE email = ? LIMIT 1', array($session_data['email']));
        		$user = $user->row();


                if (isset($_POST['submit']))
				{
					
					$data_insert = array(
						'title' => $_POST['title'],
						'status' => $_POST['status'],
						'kategori' => htmlentities($_POST['kategori']),
						'content' => $_POST['content'],
						'slug' => slug($_POST['title']),
						'date_created' => date('Y-m-d'),
						'posted_by' => $user->name	
					);

					$random = uniqid(rand());
					if (is_uploaded_file($_FILES["thumbnail"]["tmp_name"]))
					{	

						move_uploaded_file($_FILES["thumbnail"]["tmp_name"], "./uploads/berita/".$random.$_FILES["thumbnail"]["name"]);
						$data_insert['img_thumbnail'] = $random.$_FILES["thumbnail"]["name"];	
					}

					$res = $this->db->insert('tik_news', $data_insert);
					$compressed = $this->compress_image('./uploads/berita/'.$data_insert['img_thumbnail'], './uploads/berita/'.$data_insert['img_thumbnail'], 50);
					$this->session->set_flashdata('success_msg', 'Berita sudah berhasil ditambah.');
					return redirect('/admin/berita', 'refresh');


				}else{
                	$this->load->view('admin/berita/tambah_berita', array('kategori' => $kategori ));
				
				}

				
		}

		public function edit_berita($id){
			$this->auth();
			$edit_berita = $this->db->query('select * from tik_news where id='.$id);
			$kategori = $this->db->query("SELECT * FROM tik_news_category");

			$session_data = $this->session->userdata('logged_in');
        	$user = $this->db->query('SELECT * from tik_users WHERE email = ? LIMIT 1', array($session_data['email']));
    		$user = $user->row();



            if (isset($_POST['submit']))
			{
					$data_update = array(
						'title' => $_POST['title'],
						'status' => $_POST['status'],
						'kategori' => $_POST['kategori'],
						'slug' => slug($_POST['title']),
						'content' => htmlentities($_POST['content']),
						// 'img_thumbnail' => $random
						'posted_by' => $user->name

					);

					if (is_uploaded_file($_FILES["thumbnail"]["tmp_name"] ))
					{	

						foreach ($edit_berita->result_array() as $delete ) {
							@unlink("./uploads/berita/".$delete["img_thumbnail"]);
						}
						$random = uniqid(rand());
						move_uploaded_file($_FILES["thumbnail"]["tmp_name"], "./uploads/berita/".$random.$_FILES["thumbnail"]["name"]);
						$data_update['img_thumbnail'] = $random.$_FILES["thumbnail"]["name"];
					}

					$this->db->where('id', $id);
					$this->db->update('tik_news', $data_update);
					$compressed = $this->compress_image('./uploads/berita/'.$data_update['img_thumbnail'], './uploads/berita/'.$data_update['img_thumbnail'], 50);
					$this->session->set_flashdata('err_msg', 'Berita sudah berhasil diedit.');
					return redirect('/admin/edit_berita/'.$id, 'refresh');

			}

			$this->load->view('admin/berita/edit_berita', array('edit_berita' => $edit_berita, 'kategori' => $kategori));
		}

		public function delete_berita($id){
			$this->auth();
			$delete_file = $this->db->query('select img_thumbnail from tik_news where id='.$id);
			
			foreach ($delete_file->result_array() as $delete ) {
				@unlink("./uploads/berita/".$delete["img_thumbnail"]);
			}

			$res = $this->db->delete('tik_news', array('id' => $id));
			$this->session->set_flashdata('success_msg', 'Berita berhasil dihapus.');
			return redirect('/admin/berita', 'refresh');
		}

		public function event()
        {
        		$this->auth();
        		$event = $this->db->query("SELECT * FROM tik_event ORDER BY date_created DESC");
        		// foreach ($berita->result_array() as $c) {
        		// 	echo "Judul : " .$c['title']."<br .>";
        		// }
                $this->load->view('admin/event/event', array('event' => $event));
        }

        public function tambah_event()
        {	
        	$this->auth();
        	if (isset($_POST['submit']))
			{
				$new_event = array(
					'event' => $_POST['event'],
					'date_start' => $_POST['date_start'],
					'date_end' => $_POST['date_end'],
					'time' => $_POST['jam'],
					'lokasi' => $_POST['lokasi'],
					'date_created' => date('Y-m-d'),
					'slug' => slug($_POST['event']),
					'content' => htmlentities($_POST['content'])
					
				);
				$res = $this->db->insert('tik_event', $new_event);
				return redirect('/admin/event', 'refresh');
			}		
                $this->load->view('admin/event/tambah_event', array());
        }

        public function delete_event($id){
        	$this->auth();
        	$res = $this->db->delete('tik_event', array('id' => $id));
			return redirect('/admin/event', 'refresh');
        }	 

	    public function edit_event($id){
	    	$this->auth();
        	$edit_event = $this->db->query('select * from tik_event where id='.$id);

        	if (isset($_POST['submit']))
			{
					$data_update = array(
						'event' => $_POST['event'],
						'date_start' => $_POST['date_start'],
						'date_end' => $_POST['date_end'],
						'time' => $_POST['time'],
						'lokasi' => $_POST['lokasi'],
						'slug' => slug($_POST['event']),
						'content' => htmlentities($_POST['content'])

					);

					$this->db->where('id', $id);
					$this->db->update('tik_event', $data_update);
					return redirect('/admin/event', 'refresh');

			}

        	$this->load->view('admin/event/edit_event', array('edit_event' => $edit_event));
        }	

        public function page(){
        	$this->auth();
        	$page = $this->db->query("SELECT * FROM tik_pages ORDER BY id DESC");

        	// sortable

        	
        	// pagination
			$this->load->library("pagination");
			$config["base_url"] = $config["base_url"] = site_url("admin/page");
			$config["per_page"] = 7;
			$config["num_links"] = 10;
			$config["total_rows"] = $this->db->get("tik_pages")->num_rows();

			// style css costum
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close'] ="</ul>";
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
			$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
			$config['next_tag_open'] = "<li>";
			$config['next_tagl_close'] = "</li>";
			$config['prev_tag_open'] = "<li>";
			$config['prev_tagl_close'] = "</li>";
			$config['first_tag_open'] = "<li>";
			$config['first_tagl_close'] = "</li>";
			$config['last_tag_open'] = "<li>";
			$config['last_tagl_close'] = "</li>";

			$this->pagination->initialize($config);

			$this->db->order_by("id", "desc");
			$data["query"] = $this->db->get("tik_pages", $config["per_page"], $this->uri->segment(3));


        	$this->load->view('admin/page/page', $data);
        }

        public function tambah_page()
        {
        	$this->auth();	
        	if (isset($_POST['submit']))
			{
				$new_page = array(
					'page_name' => $_POST['page_name'],
					'slug_url' => slug($_POST['page_name']),
					'date_created' => date('Y-m-d'),
					'content' => htmlentities($_POST['content'])
					
				);
				$res = $this->db->insert('tik_pages', $new_page);
				$this->session->set_flashdata('success_msg', 'Page berhasil ditambah.');
				return redirect('/admin/page', 'refresh');
			}		
                $this->load->view('admin/page/tambah_page', array());
        }

        public function edit_page($id){
        	$this->auth();
        	$edit_page = $this->db->query('select * from tik_pages where id='.$id);

        	if (isset($_POST['submit']))
			{
					$data_update = array(
						'page_name' => $_POST['page_name'],
						'slug_url' => slug($_POST['page_name']),
						'content' => htmlentities($_POST['content'])

					);

					$this->db->where('id', $id);
					$this->db->update('tik_pages', $data_update);
					$this->session->set_flashdata('success_msg', 'Page berhasil edit.');
					return redirect('/admin/edit_page/'.$id, 'refresh');

			}

        	$this->load->view('admin/page/edit_page', array('edit_page' => $edit_page));
        }	

        public function delete_page($id){
        	$this->auth();
        	$res = $this->db->delete('tik_pages', array('id' => $id));
        	$this->session->set_flashdata('success_msg', 'Page berhasil dihapus.');
			return redirect('/admin/page', 'refresh');
        }

        public function slide(){
        	$this->auth();
        	$slide = $this->db->query("SELECT * FROM tik_slide ORDER BY sort");

        	

        	if (isset($_POST['data_sorted']))
				{

				$data_sent = $_POST['data_sorted'];	
				
				$data_array = explode(",", $data_sent);

	        	$count = 0;
				while ( true ) {
		        	foreach ( $data_array as $data ) {
						$this->db->where('id', $data);
						$this->db->update('tik_slide', array('sort' => $count++));
					}
			        if ( $count == count($data_array) ) break;
				}
				$this->session->set_flashdata('success_msg', 'Slide berhasil disort.');
				return redirect('/admin/slide', 'refresh');


			}

			$this->load->view('admin/slide/slide', array('slide' => $slide));

        }	

        public function tambah_slide()
        {
        	$this->auth();

        	if (isset($_POST['submit']))
			{
				$session_data = $this->session->userdata('logged_in');
	        	$user = $this->db->query('SELECT * from tik_users WHERE email = ? LIMIT 1', array($session_data['email']));
	    		$user = $user->row();	
				
				$new_slide = array(
					'slide_name' => $_POST['slide_name'],
					'redirect_to' => $_POST['redirect_to'],
					'posted_by' => $user->name
				);

				$random = uniqid(rand());
				if (is_uploaded_file($_FILES["gambar_slide"]["tmp_name"]))
				{	

					move_uploaded_file($_FILES["gambar_slide"]["tmp_name"], "./uploads/slider/".$random.$_FILES["gambar_slide"]["name"]);
					$new_slide['img_slide'] = $random.$_FILES["gambar_slide"]["name"];	
				}


				$res = $this->db->insert('tik_slide', $new_slide);
				$this->session->set_flashdata('success_msg', 'Slide berhasil ditambah.');
				return redirect('/admin/slide', 'refresh');
			}		
                $this->load->view('admin/slide/tambah_slide');
        }
				

        public function delete_slide($id){
			$this->auth();

			$delete_file = $this->db->query('select img_slide from tik_slide where id='.$id);
			
			foreach ($delete_file->result_array() as $delete ) {
				@unlink("./uploads/slider/".$delete["img_slide"]);
			}

			$res = $this->db->delete('tik_slide', array('id' => $id));
			$this->session->set_flashdata('success_msg', 'Slide berhasil dihapus.');
			return redirect('/admin/slide', 'refresh');
		}

		public function edit_slide($id){
        	$this->auth();

        	$edit_slide = $this->db->query('select * from tik_slide where id='.$id);
        	$edit_slide = $edit_slide->row();


        	// get user
        	$session_data = $this->session->userdata('logged_in');
        	$user = $this->db->query('SELECT * from tik_users WHERE email = ? LIMIT 1', array($session_data['email']));
    		$user = $user->row();

        	if (isset($_POST['submit']))
			{
					$data_update = array(
						'slide_name' => $_POST['slide_name'],
						'redirect_to' => $_POST['redirect_to'],
						'posted_by' => $user->name

					);

					$random = uniqid(rand());
					if (is_uploaded_file($_FILES["gambar_slide"]["tmp_name"]))
					{	
						@unlink("./uploads/slider/".$edit_slide->img_slide);	
						move_uploaded_file($_FILES["gambar_slide"]["tmp_name"], "./uploads/slider/".$random.$_FILES["gambar_slide"]["name"]);
						$data_update['img_slide'] = $random.$_FILES["gambar_slide"]["name"];	
					}


					$this->db->where('id', $id);
					$this->db->update('tik_slide', $data_update);
					$this->session->set_flashdata('success_msg', 'Slide sudah berhasil diedit.');
					return redirect('/admin/edit_slide/'.$id, 'refresh');

			}

        	$this->load->view('admin/slide/edit_slide', array('edit_slide' => $edit_slide));
        }	


        public function settings(){
			$this->auth();

			$settings = $this->db->query('SELECT * from tik_settings WHERE setting_name = ? ', array(1));
	        $settings = $settings->row();

	        print_r($settings);

			// $this->load->view('admin/settings/settings', array('settings' => $settings));
		}

		public function testimonial(){
        	$this->auth();
        	$testimonial = $this->db->query("SELECT * FROM tik_testimonial ORDER BY sort");

        	if (isset($_POST['data_sorted']))
				{

				$data_sent = $_POST['data_sorted'];	
				
				$data_array = explode(",", $data_sent);

	        	$count = 0;
				while ( true ) {
		        	foreach ( $data_array as $data ) {
						$this->db->where('id', $data);
						$this->db->update('tik_testimonial', array('sort' => $count++));
					}
			        if ( $count == count($data_array) ) break;
				}
				$this->session->set_flashdata('success_msg', 'Testimonial berhasil disort.');
				return redirect('/admin/testimonial', 'refresh');

			}
        	$this->load->view('admin/testimonial/testimonial', array('testimonial' => $testimonial));
        }

        public function tambah_testimonial()
        {
        	$this->auth();	
        	if (isset($_POST['submit']))
			{
				$new_testimonial = array(
					'author' => $_POST['author'],
					'testimonial' => $_POST['testimonial'],
					'date_created' => date('Y-m-d'),
					'job_and_position' => $_POST['jabatan']
					
				);

				$random = uniqid(rand());
				if (is_uploaded_file($_FILES["thumbnail"]["tmp_name"]))
				{	

					move_uploaded_file($_FILES["thumbnail"]["tmp_name"], "./uploads/testimonial/".$random.$_FILES["thumbnail"]["name"]);
					$new_testimonial['img_profile_author'] = $random.$_FILES["thumbnail"]["name"];	
				}

				$res = $this->db->insert('tik_testimonial', $new_testimonial);
				
				$this->session->set_flashdata('success_msg', 'Testimonial berhasil ditambah.');
				return redirect('/admin/testimonial', 'refresh');
			}		
                $this->load->view('admin/testimonial/tambah_testimonial', array());
        }

        public function delete_testimonial($id){
			$this->auth();

			$delete_testimonial = $this->db->query('select img_profile_author from tik_testimonial where id='.$id);
			
			foreach ($delete_testimonial->result_array() as $delete ) {
				@unlink("./uploads/testimonial/".$delete["img_profile_author"]);
			}

			$res = $this->db->delete('tik_testimonial', array('id' => $id));
			$this->session->set_flashdata('success_msg', 'Testimonial berhasil dihapus.');
			return redirect('/admin/testimonial', 'refresh');
		}


		public function edit_testimonial($id){
        	$this->auth();

        	$edit_testimonial = $this->db->query('select * from tik_testimonial where id='.$id);
        	$edit_testimonial = $edit_testimonial->row();


        	// get user
        	$session_data = $this->session->userdata('logged_in');
        	$user = $this->db->query('SELECT * from tik_users WHERE email = ? LIMIT 1', array($session_data['email']));
    		$user = $user->row();

        	if (isset($_POST['submit']))
			{
					$edit_testimonial = array(
						'author' => $_POST['author'],
						'testimonial' => $_POST['testimonial'],
						'date_created' => date('Y-m-d'),
						'job_and_position' => $_POST['jabatan']
						
					);

					$random = uniqid(rand());
					if (is_uploaded_file($_FILES["thumbnail"]["tmp_name"]))
					{	
						@unlink("./uploads/testimonial/".$edit_testimonial->img_profile_author);	
						move_uploaded_file($_FILES["thumbnail"]["tmp_name"], "./uploads/testimonial/".$random.$_FILES["thumbnail"]["name"]);
						$edit_testimonial['img_profile_author'] = $random.$_FILES["thumbnail"]["name"];
					}


					$this->db->where('id', $id);
					$this->db->update('tik_testimonial', $edit_testimonial);
					$this->session->set_flashdata('success_msg', 'Testimonial berhasil diubah.');
					return redirect('/admin/edit_testimonial/'.$id, 'refresh');

			}

        	$this->load->view('admin/testimonial/edit_testimonial', array('edit_testimonial' => $edit_testimonial));
        }

        public function fasilitas_dan_layanan(){
        	$this->auth();
        	$facilities = $this->db->query("SELECT * FROM tik_facilities_and_services ORDER BY sort");

        	if (isset($_POST['data_sorted']))
				{

				$data_sent = $_POST['data_sorted'];	
				
				$data_array = explode(",", $data_sent);

	        	$count = 0;
				while ( true ) {
		        	foreach ( $data_array as $data ) {
						$this->db->where('id', $data);
						$this->db->update('tik_facilities_and_services', array('sort' => $count++));
					}
			        if ( $count == count($data_array) ) break;
				}

				return redirect('/admin/fasilitas_dan_layanan', 'refresh');
				

			}

        	$this->load->view('admin/facilities_and_services/fasilitas_dan_layanan', array('facilities' => $facilities));
        }	


        public function edit_fasilitas_dan_layanan($id){
        	$this->auth();

    		$session_data = $this->session->userdata('logged_in');
    		$edit_facilities = $this->db->query('SELECT * from tik_facilities_and_services WHERE id='.$id);
    		$edit_facilities = $edit_facilities->row();

    		if (isset($_POST['submit']))
    		{
    			$data_update = array(
    				'fasilitas_dan_layanan' => $_POST['fasilitas_dan_layanan'],
    				'description' => $_POST['content'],
    				'add_ons' => $_POST['add_ons']
    			);
    			

				if (is_uploaded_file($_FILES["image_1"]["tmp_name"])) 
				{
					@unlink("./uploads/facilities_and_services/".$edit_facilities->image_1);
					$random1 = uniqid(rand());
					move_uploaded_file($_FILES["image_1"]["tmp_name"], "./uploads/facilities_and_services/".$random1.$_FILES["image_1"]["name"]);
					$data_update['image_1'] = $random1.$_FILES["image_1"]["name"];
				}

				if (is_uploaded_file($_FILES["image_2"]["tmp_name"])) 
				{
					@unlink("./uploads/facilities_and_services/".$edit_facilities->image_2);
					$random2 = uniqid(rand());
					move_uploaded_file($_FILES["image_2"]["tmp_name"], "./uploads/facilities_and_services/".$random2.$_FILES["image_2"]["name"]);
					$data_update['image_2'] = $random2.$_FILES["image_2"]["name"];
				}

				if (is_uploaded_file($_FILES["image_3"]["tmp_name"])) 
				{
					@unlink("./uploads/facilities_and_services/".$edit_facilities->image_3);
					$random3 = uniqid(rand());
					move_uploaded_file($_FILES["image_3"]["tmp_name"], "./uploads/facilities_and_services/".$random3.$_FILES["image_3"]["name"]);
					$data_update['image_3'] = $random3.$_FILES["image_3"]["name"];
				}

				if (is_uploaded_file($_FILES["image_4"]["tmp_name"])) 
				{
					@unlink("./uploads/facilities_and_services/".$edit_facilities->image_4);
					$random4 = uniqid(rand());
					move_uploaded_file($_FILES["image_4"]["tmp_name"], "./uploads/facilities_and_services/".$random4.$_FILES["image_4"]["name"]);
					$data_update['image_4'] = $random4.$_FILES["image_4"]["name"];
				}


				$this->db->where('id', $id);
				$this->db->update('tik_facilities_and_services', $data_update);
				$this->session->set_flashdata('scc_msg', 'Fasilitas dan Layanan berhasil diubah.');
				return redirect('/admin/edit_fasilitas_dan_layanan/'.$id, 'refresh');

    		}

        	$this->load->view('admin/facilities_and_services/edit_fasilitas_dan_layanan', array('edit_facilities' => $edit_facilities));
        }

        public function delete_fasilitas_dan_layanan($id){
        	$this->auth();
			$image_1 = $this->db->query('select image_1 from tik_facilities_and_services where id='.$id);
			$image_2 = $this->db->query('select image_2 from tik_facilities_and_services where id='.$id);
			$image_3 = $this->db->query('select image_3 from tik_facilities_and_services where id='.$id);
			$image_4 = $this->db->query('select image_4 from tik_facilities_and_services where id='.$id);
			
			@unlink("./uploads/facilities_and_services/".$image_1->row()->image_1);
			@unlink("./uploads/facilities_and_services/".$image_2->row()->image_2);
			@unlink("./uploads/facilities_and_services/".$image_3->row()->image_3);
			@unlink("./uploads/facilities_and_services/".$image_4->row()->image_4);

			$res = $this->db->delete('tik_facilities_and_services', array('id' => $id));
			$this->session->set_flashdata('success_msg', 'Fasilitas dan Layanan berhasil dihapus.');
			return redirect('/admin/fasilitas_dan_layanan', 'refresh');

        }

        public function tambah_fasilitas_dan_layanan(){
        	$this->auth();

    		$session_data = $this->session->userdata('logged_in');
    		$user = $this->db->query('SELECT * from tik_users WHERE email = ? LIMIT 1', array($session_data['email']));
    		$user = $user->row();


            if (isset($_POST['submit']))
			{
				
				$data_insert = array(
					'fasilitas_dan_layanan' => $_POST['fasilitas_dan_layanan'],
					'description' => $_POST['content'],
					'slug_url' => slug($_POST['fasilitas_dan_layanan']),
					'date_created' => date('Y-m-d'),
					'posted_by' => $user->name,
					'add_ons' => $_POST['add_ons']
				);

				if (is_uploaded_file($_FILES["image_1"]["tmp_name"]))

				{	
					$random1 = uniqid(rand());
					move_uploaded_file($_FILES["image_1"]["tmp_name"], "./uploads/facilities_and_services/".$random1.$_FILES["image_1"]["name"]);
					$data_insert['image_1'] = $random1.$_FILES["image_1"]["name"];	

				}

				if (is_uploaded_file($_FILES["image_2"]["tmp_name"]))

				{	
					$random2 = uniqid(rand());
					move_uploaded_file($_FILES["image_2"]["tmp_name"], "./uploads/facilities_and_services/".$random2.$_FILES["image_2"]["name"]);
					$data_insert['image_2'] = $random2.$_FILES["image_2"]["name"];	

				}

				if (is_uploaded_file($_FILES["image_3"]["tmp_name"]))

				{	
					$random3 = uniqid(rand());
					move_uploaded_file($_FILES["image_3"]["tmp_name"], "./uploads/facilities_and_services/".$random3.$_FILES["image_3"]["name"]);
					$data_insert['image_3'] = $random3.$_FILES["image_3"]["name"];	

				}

				if (is_uploaded_file($_FILES["image_4"]["tmp_name"]))

				{	
					$random4 = uniqid(rand());
					move_uploaded_file($_FILES["image_4"]["tmp_name"], "./uploads/facilities_and_services/".$random4.$_FILES["image_4"]["name"]);
					$data_insert['image_4'] = $random4.$_FILES["image_4"]["name"];	

				}



				$res = $this->db->insert('tik_facilities_and_services', $data_insert);
				return redirect('/admin/fasilitas_dan_layanan', 'refresh');


			}else{
            	$this->load->view('admin/facilities_and_services/tambah_fasilitas_dan_layanan', array() );
			
			}
        }

        public function struktur_organisasi(){
        	$this->auth();
        	$member = $this->db->query("SELECT * FROM tik_devision_member ORDER BY sort");

        	if (isset($_POST['data_sorted']))
				{

				$data_sent = $_POST['data_sorted'];	
				
				$data_array = explode(",", $data_sent);

	        	$count = 0;
				while ( true ) {
		        	foreach ( $data_array as $data ) {
						$this->db->where('id', $data);
						$this->db->update('tik_devision_member', array('sort' => $count++));
					}
			        if ( $count == count($data_array) ) break;
				}

				$this->session->set_flashdata('success_msg', 'Karyawan berhasil disort.');
				return redirect('/admin/struktur_organisasi', 'refresh');
				

			}

        	$this->load->view('admin/struktur-organisasi/daftar-karyawan', array('member' => $member));
        }

        public function delete_karyawan($id){
        	$this->auth();
        	$get_karyawan = $this->db->query('select photo_profile from tik_devision_member where id='.$id);
        	@unlink("./uploads/devision/".$get_karyawan->row()->photo_profile);

        	$res = $this->db->delete('tik_devision_member', array('id' => $id));
			$this->session->set_flashdata('success_msg', 'Karyawan berhasil dihapus.');
			return redirect('/admin/struktur_organisasi', 'refresh');
        }

        public function tambah_karyawan(){
        	$this->auth();
        	$devision = $this->db->query("SELECT * FROM tik_devision_category");
        	if (isset($_POST['submit']))
			{
				$add_karyawan = array(
					'name' => $_POST['nama'],
					'devision' => $_POST['devision'],
					'website' => $_POST['website']
				);

				$random = uniqid(rand());
				if (is_uploaded_file($_FILES["thumbnail"]["tmp_name"]))
				{	

					move_uploaded_file($_FILES["thumbnail"]["tmp_name"], "./uploads/devision/".$random.$_FILES["thumbnail"]["name"]);
					$add_karyawan['photo_profile'] = $random.$_FILES["thumbnail"]["name"];	
				}
				$res = $this->db->insert('tik_devision_member', $add_karyawan);
				$this->session->set_flashdata('success_msg', 'Karyawan berhasil ditambah.');
				return redirect('/admin/struktur_organisasi', 'refresh');
			}
        	$this->load->view('admin/struktur-organisasi/tambah-karyawan', array('devision' => $devision));
        }

        public function edit_karyawan($id){
        	$this->auth();

        	$devision = $this->db->query("SELECT * FROM tik_devision_category");
        	$get_karyawan = $this->db->query('select * from tik_devision_member where id='.$id);
        	$get_karyawan = $get_karyawan->row();


        	// get user
        	$session_data = $this->session->userdata('logged_in');
        	$user = $this->db->query('SELECT * from tik_users WHERE email = ? LIMIT 1', array($session_data['email']));
    		$user = $user->row();



        	if (isset($_POST['submit']))
			{
					$edit_karyawan = array(
						'name' => $_POST['nama'],
						'devision' => $_POST['devision'],
						'website' => $_POST['website']
					);

					$random = uniqid(rand());
					if (is_uploaded_file($_FILES["thumbnail"]["tmp_name"]))
					{	
						@unlink("./uploads/devision/".$get_karyawan->photo_profile);	
						move_uploaded_file($_FILES["thumbnail"]["tmp_name"], "./uploads/devision/".$random.$_FILES["thumbnail"]["name"]);
						$edit_karyawan['photo_profile'] = $random.$_FILES["thumbnail"]["name"];
					}


					$this->db->where('id', $id);
					$this->db->update('tik_devision_member', $edit_karyawan);
					$this->session->set_flashdata('success_msg', 'Karyawan berhasil diedit.');
					return redirect('/admin/edit_karyawan/'.$id, 'refresh');

			}

        	$this->load->view('admin/struktur-organisasi/edit-karyawan', array('get_karyawan' => $get_karyawan, 'devision' => $devision ));
        }



}