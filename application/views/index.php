<!DOCTYPE html>
<html>
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title>UPT TIK (Teknologi Informasi dan Komunikasi) - Universitas Syiah Kuala </title> 

        <meta name="keywords" content="UPT TIK (Teknologi Informasi dan Komunikasi) - Universitas Syiah Kuala" />
        <meta name="description" content="UPT TIK (Teknologi Informasi dan Komunikasi) - Universitas Syiah Kuala ">
        <meta name="author" content="UPT TIK, Unsyiah">

        <!-- Favicon -->
        <link href="<?php echo base_url('assets/img/favicon.png') ?>" rel="shortcut icon" type="image/png" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:500,400italic,700italic,300,100,700,500italic,400%7CMaterial+Icons+Extended%7COpen+Sans:300,400,600,700%7CProduct+Sans:400&lang=id" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css') ?>" >
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/simple-line-icons/css/simple-line-icons.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/owl.carousel/assets/owl.carousel.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/owl.carousel/assets/owl.theme.default.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/magnific-popup/magnific-popup.min.css') ?>">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-elements.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-blog.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-shop.css"') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-animate.css') ?>">

        <!-- Current Page CSS -->
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/settings.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/layers.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/navigation.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/circle-flip-slideshow/css/component.css') ?>" >

        <!-- Skin CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/default.css') ?>">

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>">


        <!-- Head Libs -->
        <script src="assets/vendor/modernizr/modernizr.min.js"></script>

        <style type="text/css">
        .owl-carousel.show-nav-title .owl-nav {
        }
        </style>

    </head>
    <body>
        <div class="body">
            <header id="header" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 57, "stickySetTop": "-57px", "stickyChangeLogo": true}'>
                <div class="header-body">
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-logo">
                                    <a href="<?php echo site_url('') ?>">
                                        <img alt="UPT TIK - UNSYIAH" width="315" height="80"  data-sticky-width="230" data-sticky-height="55" data-sticky-top="40" src="<?php echo base_url ('assets/img/logo.png') ?>">
                                    </a>
                                </div>
                            </div>
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-search hidden-xs">
                                        <form id="searchForm" action="page-search-results.html" method="get">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" id="q" placeholder="Search..." required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                    <nav class="header-nav-top hidden-xs">
                                        <ul class="nav nav-pills">
                                            <li class="hidden-xs">
                                                <a href="<?php echo site_url('contact') ?>"><i class="fa fa-angle-right"></i> Contact Us</a>
                                            </li>
                                            <li>
                                                <span class="ws-nowrap"><i class="fa fa-phone"></i> 0651-6303969</span>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="header-row">
                                    <div class="header-nav">
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                        <!-- <ul class="header-social-icons social-icons hidden-xs">
                                            <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                            <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                            <li class="social-icons-youtube"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-youtube"></i></a></li>
                                        </ul> -->
                                        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                            <nav>
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <li class="dropdown">
                                                        <a class="" href="<?php echo site_url('') ?>">
                                                            Beranda
                                                        </a>
                                                    </li>

                                                    <li class="dropdown">
                                                        <a class="dropdown-toggle" href="#">
                                                            Profil UPT TIK
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?php echo site_url('page/detail/kepala-upt-tik') ?>">Kepala UPT TIK</a></li>
                                                            <li><a href="<?php echo site_url('page/detail/visi-dan-misi') ?>">Visi & Misi</a></li>
                                                            <li><a href="<?php echo site_url('/strukturorganisasi') ?>#semua">Struktur Organisasi</a></li>
                                                            <li><a href="<?php echo site_url('page/detail/sejarah') ?>">Sejarah</a></li>
                                                        </ul>
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('berita/') ?>">
                                                            Berita
                                                        </a>
                                                        
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('sisteminformasi') ?>">
                                                            Sistem Informasi
                                                        </a>
                                                    </li>

                                                    <li class="dropdown dropdown-mega">
                                                        <a href="<?php echo site_url('fasilitasdanlayanan') ?>#semua">
                                                            Fasilitas dan Layanan
                                                        </a>
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('contact') ?>">
                                                            Kontak Kami
                                                        </a>
                                                    </li>

                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>


            <div role="main" class="main">
                
                    <div class="slider-container rev_slider_wrapper" style="height: 700px;">
                        <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 800, "gridheight": 410}'>
                            <ul>

                            <?php foreach ($slide->result_array() as $data) { ?>
                            <?php if ($data['redirect_to']): ?>
                                <li data-transition="fade">
                                         
                                        <img src="<?php echo base_url('/uploads/slider/')?>/<?php echo $data['img_slide'] ?>"
                                            alt=""
                                            data-bgposition="center center" 
                                            data-bgfit="cover" 
                                            data-bgrepeat="no-repeat"
                                            class="rev-slidebg">
                                </li>

                            <?php elseif ($data['redirect_to'] == ''): ?>
                                
                                <li data-transition="fade">
                                    <img src="<?php echo base_url('/uploads/slider/')?>/<?php echo $data['img_slide'] ?>"
                                        alt=""
                                        data-bgposition="center center" 
                                        data-bgfit="cover" 
                                        data-bgrepeat="no-repeat" 
                                        class="rev-slidebg">
                                </li>    

                            <?php endif; ?>    
                            <?php } ?> 
                                
                            </ul>
                        </div>
                    </div>


                <div class="home-intro" id="home-intro">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-8">
                                <p>
                                    
                                    Meningkatkan Kualitas Universitas melalui <em>ICT</em>
                                    <span>Enhancing University Performance through ICT</span>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <div class="mt-lg mb-xl">
                                    <a href="<?php echo site_url('sisteminformasi') ?>" data-hash class="btn btn-primary mr-md appear-animation" data-appear-animation="fadeInDown" data-appear-animation-delay="300">LIHAT SISTEM INFORMASI</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="container">

                    <div class="row">
                        <div class="col-md-12 center">
                            <h3 class="word-rotator-title mb-sm "><b>VISI UPT TIK : </b> MENJADI PUSAT <strong><span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
                                <span class="word-rotate-items">
                                    <span>PENGEMBANGAN</span>
                                    <span>PENGELOLAAN</span>
                                    <span>PELAYANAN</span>
                                </span>
                            </span></strong> TEKNOLOGI INFORMASI <br> DAN KOMUNIKASI YANG TERKEMUKA SECARA NASIONAL
                            </h3>
                            <p class="lead">UPT TIK Sangat Berperan dalam Meningkatkan Kualitas Universitas Syiah Kuala melalui <b>ICT</b></p>
                            
                        </div>
                    </div>

                    <div class="row mt-xl">
                        <div class="counters counters-text-dark">
                            <div class="col-md-3 col-sm-6">
                                <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
                                    <i class="fa fa-desktop"></i>
                                    <strong data-to="21" data-append="+">0</strong>
                                    <label>Sistem Informasi</label>
                                    <p class="text-color-primary mb-xl">Berbasis Website</p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600">
                                    <i class="fa fa-building-o"></i>
                                    <strong data-to="11">0</strong>
                                    <label>Fasilitas dan Layanan</label>
                                    <p class="text-color-primary mb-xl">di Gedung ICT Unsyiah</p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="900">
                                    <i class="fa fa-server"></i>
                                    <strong data-to="50" data-append="+">0</strong>
                                    <label>Available Server</label>
                                    <p class="text-color-primary mb-xl">Pengembangan Teknologi</p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="1200">
                                    <i class="fa fa-users"></i>
                                    <strong data-to="25" data-append="+">0</strong>
                                    <label>Tim UPT TIK</label>
                                    <p class="text-color-primary mb-xl">Memberikan Pelayanan Prima</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-5">
                                    <iframe width="460" height="270" src="https://www.youtube.com/embed/9kjz2bmAl8w" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-md-7">
                                    <h3 class="mt-xl">Selamat Datang di <strong>UPT TIK - UNSYIAH</strong></h3>
                                    <p>
                                        Keberadaan UPT TIK mengambil posisi penting dalam pengembangan teknologi sesuai dengan tuntutan Visi Universitas Syiah Kuala, Sehingga menghasilkan lulusan yang berkualitas yang menjunjung nilai-nilai moral dan etika”. Terkait pengembangan teknologi, Unsyiah mendirikan UPT TIK (Teknologi Informasi dan Komunikasi) untuk menunjang pelaksanaan visi dan misi universitas.

                                </p></div>
                            </div>
                    </div>
                </section>
            </div>
            <br><br><br>    
            <section class="container">
                <div class="row">
                    <div class="col-md-6">
                            <div class="heading heading-border heading-middle-border ">
                                <h2>berita <strong>terbaru</strong></h2>
                            </div>

                            <div class="owl-carousel owl-theme show-nav-title" data-plugin-options='{"responsive": {"0": {"items": 1}, "479": {"items": 1}, "768": {"items": 2}, "979": {"items": 3}, "1199": {"items": 2}}, "items": 2, "margin": 10, "loop": false, "nav": true, "dots": false}'>
                                <?php foreach ($berita_terbaru->result_array() as $data) { ?>
                                <div>
                                    <div class="recent-posts">
                                        <article class="post">
                                            <h4 class="heading-primary" style="text-transform: uppercase;"><a href="<?php echo site_url('berita/detail') ?>/<?php echo $data['slug'] ?>"><?php echo $data['title'] ?></a></h4>
                                            <p><?php echo word_limiter(strip_tags(html_entity_decode($data['content'])), 20);  ?> <a href="<?php echo site_url('berita/detail') ?>/<?php echo $data['slug'] ?>" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
                                        </article>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                    </div>
                    <div class="col-md-6">
                             <div class="heading heading-border heading-middle-border">
                                <h2>pengumuman <strong>terbaru</strong></h2>
                            </div>

                           <div class="owl-carousel owl-theme show-nav-title show-nav-title-pengunguman " data-plugin-options='{"responsive": {"0": {"items": 1}, "479": {"items": 1}, "768": {"items": 2}, "979": {"items": 3}, "1199": {"items": 2}}, "items": 2, "margin": 10, "loop": false, "nav": true, "dots": false}'>
                                <?php foreach ($pengunguman->result_array() as $data) { ?>
                                <div>
                                    <div class="recent-posts">
                                        <article class="post">
                                            
                                            <h4 class="heading-primary" style="text-transform: uppercase;"><a href="<?php echo site_url('berita/detail') ?>/<?php echo $data['slug'] ?>"><?php echo $data['title'] ?></a></h4>
                                            <p><?php echo word_limiter(strip_tags(html_entity_decode($data['content'])), 20);  ?> <a href="<?php echo site_url('berita/detail') ?>/<?php echo $data['slug'] ?>" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
                                        </article>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                    </div>
                </div>  
                </section>
                <br><br>



                <section class="parallax section section-text-light section-parallax section-center mt-none mb-none" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo base_url('assets/img/parallax-landing.jpg') ?>);">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options='{"items": 1, "loop": false, "autoplay": true}'>
                                    <?php foreach ($testimonial->result_array() as $data) { ?>
                                    <div>
                                        <div class="col-md-12">
                                            <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
                                                <div class="testimonial-author">
                                                    <img src="<?php echo base_url('./uploads/testimonial/')?>/<?php echo $data['img_profile_author'] ?>" class="img-responsive img-circle" alt="">
                                                </div>
                                                <blockquote>
                                                    <p><?php echo $data['testimonial'] ?></p>
                                                </blockquote>
                                                <div class="testimonial-author">
                                                    <p><strong><?php echo $data['author'] ?></strong><span><?php echo $data['job_and_position'] ?></span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>    

             <br><br><br> <br>   

            <div class="container">
                <div class="heading heading-border heading-middle-border heading-middle-border-center center">
                                <h4>UPT TIK PARTNERS</h4>
                            </div><br>

                            <div class="owl-carousel owl-theme " data-plugin-options='{"items": 6, "itemsMobile" : 3, "autoplay": true, "autoplayTimeout": 6000, "pagination" : true, "dots" : false}'>
                                <div>
                                    <img style="width: 130px;" class="img-responsive" src="<?php echo base_url('assets/img/partners/telkom.png') ?>">
                                </div>
                                <div>
                                    <img style="width: 100px;" class="img-responsive" src="<?php echo base_url('assets/img/partners/ADOC.jpg') ?>">
                                </div>

                                  <div>
                                    <img style="width: 130px;"  class="img-responsive" src="<?php echo base_url('assets/img/partners/mandiri.gif') ?>">
                                </div>
                                  <div>
                                    <img style="width: 130px;"  class="img-responsive" src="<?php echo base_url('assets/img/partners/bni.png') ?>">
                                </div>
                                  <div>
                                    <img style="width: 130px;"  class="img-responsive" src="<?php echo base_url('assets/img/partners/bri.png') ?>">
                                </div>
                                  <div>
                                    <img style="width: 130px;"  class="img-responsive" src="<?php echo base_url('assets/img/partners/bank-aceh.png') ?>">
                                </div>
                                  <div>
                                    <img style="width: 130px;"  class="img-responsive" src="<?php echo base_url('assets/img/partners/bank-btn.png') ?>">
                                </div>
                            </div>

            </div>

                
            <div class="container">
                    <!-- <div class="row mt-xl mb-xl">
                        <div class="col-md-5">
                            <img class="img-responsive mt-xl appear-animation fadeInLeft appear-animation-visible" src="assets/img/top-quality-documentation.png" alt="" data-appear-animation="fadeInLeft">
                        </div>
                        <div class="col-md-7">
                            <h2 class="mt-xl">Selamat Datang di <strong>UPT TIK UNSYIAH</strong></h2>
                            <p>
                                Porto comes with a very extensive and as thorough as possible documentation where each section of the theme and its features are described in a easy way to understand, it also comes with a lot of screenshots to help you. If the documentation is not enough contact us on our support forum.
                        </p></div>
                    </div> -->
                </div>  
            

            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="footer-ribbon">
                            <span>Get in Touch</span>
                        </div>
                        <div class="col-md-3">
                            <div class="newsletter">
                                <h4 class="footer-underline">Tentang Kami</h4>
                                <ul class="tentang-kami">
                                    <li><a href="<?php echo site_url('page/detail/kepala-upt-tik') ?>">Kepala UPT TIK</a></li>
                                    <li><a href="<?php echo site_url('page/detail/visi-dan-misi') ?>">Visi & Misi</a></li>
                                    <li><a href="<?php echo site_url('/strukturorganisasi') ?>#semua">Struktur Organisasi</a></li>
                                    <li><a href="<?php echo site_url('page/detail/sejarah') ?>">Sejarah</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="contact-details">
                                <h4 class="footer-underline">Kantor Resmi</h4>
                                <ul class="contact">
                                    <li><p><i class="fa fa-map-marker"></i> <strong>Alamat:</strong> Gedung ICT Center Unsyiah - Taiwan <br>Jln. Teuku Nyak Arief Darussalam, Banda Aceh. </p></li>
                                    <li><p><i class="fa fa-phone"></i> <strong>HP:</strong> 0651-6303969</p></li>
                                    <li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:helpdesk.ict@unsyiah.ac.id">helpdesk.ict@unsyiah.ac.id</a></p></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <h4 class="footer-underline">Latest Tweets</h4>
                                <a class="twitter-timeline" height="70" data-theme="dark" data-src-2x="false" data-tweet-limit="2" data-chrome="noscrollbar noheader nofooter transparent " href="https://twitter.com/ictunsyiah" data-widget-id="701963310241943552">Tweets by @ictunsyiah</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                        
                        <div class="col-md-2">
                            <h4 class="footer-underline">Follow Us</h4>
                            <ul class="social-icons">
                                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-icons-twitter"><a href="https://twitter.com/ictunsyiah" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1">
                                <a href="<?php echo site_url('') ?>" class="logo">
                                    <img alt="UPT TIK - UNSYIAH" class="img-responsive" src="<?php echo base_url ('assets/img/logo-white-font.png') ?>">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <p>© Copyright 2016. UPT TIK (Teknologi Informasi dan Komunikasi) Unsyiah</p>
                            </div>
                            <div class="col-md-4">
                                <nav id="sub-menu">
                                    <ul>
                                        <li><a href="page-faq.html">FAQ's</a></li>
                                        <li><a href="sitemap.html">Sitemap</a></li>
                                        <li><a href="contact-us.html">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

       <!-- Vendor -->
        <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.appear/jquery.appear.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.easing/jquery.easing.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery-cookie/jquery-cookie.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/common/common.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.validation/jquery.validation.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.stellar/jquery.stellar.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.gmap/jquery.gmap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.lazyload/jquery.lazyload.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/isotope/jquery.isotope.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/owl.carousel/owl.carousel.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/magnific-popup/jquery.magnific-popup.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/vide/vide.min.js') ?>"></script>
        
        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo base_url('assets/js/theme.js') ?>"></script>
        
        <!-- Current Page Vendor and Views -->
        <script src="<?php echo base_url('assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/views/view.home.js') ?>"></script>
        
        <!-- Theme Custom -->
        <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
        
        <!-- Theme Initialization Files -->
        <script src="<?php echo base_url('assets/js/theme.init.js') ?>"></script>

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->

    </body>
</html>