<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>TIK Unsyiah</title>
    <!-- Bootstrap Styles-->
    <link href="<?php echo base_url('assets/admin/css/bootstrap.css') ?>" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="<?php echo base_url()?>assets/admin/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="<?php echo base_url()?>assets/admin/js/morris/morris-0.4.3.min.css" rel="stylesheet" />

    <!-- Custom Styles-->
    <link href="<?php echo base_url()?>assets/admin/css/custom-styles.css" rel="stylesheet" />
    <!-- Datepicker -->
    <link href="<?php echo base_url()?>assets/bootstrap-datepicker-1.5.1-dist/css/bootstrap-datepicker.min.css
" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/js/Lightweight-Chart/cssCharts.css"> 
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('admin/dashboard') ?>"><strong>Administrator TIK</strong></a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
               
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo site_url('admin/user_profile') ?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('admin/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
		<div id="sideNav" href=""><!-- <i class="fa fa-caret-right"></i> --></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">


                    <li>
                        <a href="<?php echo site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                    <li>
                        <a  href="#"><i class="fa fa-info"></i> Berita<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level ">
                            <li>
                                <a href="<?php echo site_url('admin/berita') ?>">Lihat Berita</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_berita') ?>">Tambah Berita</a>
                            </li>
                            
                            
                        </ul>
                    </li>


                     <li>
                        <a class="active-menu" href="#"><i class="fa fa-calendar"></i> Event<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse in">
                           <li>
                                <a href="<?php echo site_url('admin/event') ?>">Lihat Event</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_event') ?>">Tambah Event</a>
                            </li>
                            
                            
                        </ul>
                    </li>



                    <li>
                        <a href="#"><i class="fa fa-file-text"></i> Pages<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin/page') ?>">Lihat Pages</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_page') ?>">Tambah Pages</a>
                            </li>
                            
                            
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-eye"></i> Slide<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin/slide') ?>">Lihat Slide</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_slide') ?>">Tambah Slide</a>
                            </li>
                            
                            
                        </ul>
                    </li>

                    <li>
                        <a  href="#"><i class="fa fa-quote-left"></i> Testimonial<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin/testimonial') ?>">Lihat Testimonial</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_testimonial') ?>">Tambah Testimonial</a>
                            </li>
                            
                            
                        </ul>
                    </li>
                    
                    <li>
                        <a href="#"><i class="fa fa-shield"></i> Fasilitas dan Layanan<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin/fasilitas_dan_layanan') ?>">Fasilitas dan Layanan</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_fasilitas_dan_layanan') ?>">Tambah Fasilitas dan Layanan</a>
                            </li>
                            
                            
                        </ul>
                    </li>

                     <li>
                        <a href="#"><i class="fa fa-quote-left"></i> Karyawan <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin/struktur_organisasi') ?>">Lihat Karyawan</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_karyawan') ?>">Tambah Karyawan</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
      
		<div id="page-wrapper">
		  <div class="header"> 
                        <h1 class="page-header">
                            Admin Event 
                        </h1>
						<ol class="breadcrumb">
					  <li><a href="<?php echo site_url('admin/dashboard') ?>">Home</a></li>
                      <li class="active">Event</li>
					</ol> 
									
		</div>
            <div id="page-inner">

                <!-- /. ROW  -->

                <div class="row">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Tambah Event
                        </div>

                        <div class="panel-body">
                        <?php echo form_open_multipart(); ?>
                            <div class="form-group">
                              <label>Nama Event</label>
                              <input type="text" name="event" class="form-control" required>
                            </div>


                            <div class="row">
                                <div class="col-md-3">
                                    <label>Tanggal Mulai</label>
                                    <input type="text" name="date_start" class="datepicker form-control" data-date-format="yyyy-mm-dd" required>
                                </div>
                                <div class="col-md-3">
                                    <label>Tanggal Selesai</label>
                                    <input type="text" name="date_end" class="datepicker form-control" data-date-format="yyyy-mm-dd"  required>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                      <label>Jam</label>
                                      <input type="text" name="jam" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                   <label>Lokasi</label>
                                   <input type="text" name="lokasi" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Content</label><br>
                                 <textarea name="content" class="form-control" rows="2" cols="20">
                                 
                                </textarea>
                            </div>

                            <hr>
                            

                            <button type="submit" name="submit" class="btn btn-default">Publish Event</button>
                        </form>
                        </div>
                    </div>

                </div>
				
				
				
			
		
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="<?php echo base_url()?>assets/admin/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?php echo base_url()?>assets/admin/js/bootstrap.min.js"></script>
	 
    <!-- Metis Menu Js -->f
    <script src="<?php echo base_url()?>assets/admin/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="<?php echo base_url()?>assets/admin/js/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/morris/morris.js"></script>
	
	
	<script src="<?php echo base_url()?>assets/admin/js/easypiechart.js"></script>
	<script src="<?php echo base_url()?>assets/admin/js/easypiechart-data.js"></script>
	
	 <script src="<?php echo base_url()?>assets/admin/js/Lightweight-Chart/jquery.chart.js"></script>
	
    <!-- Custom Js -->
    <script src="<?php echo base_url()?>assets/admin/js/custom-scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap-datepicker-1.5.1-dist/js/bootstrap-datepicker.min.js"></script>


      
      <script>
         CKEDITOR.replace( 'content',
         {
            filebrowserBrowseUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
        });

         $('.datepicker').datepicker({
        });
      </script>

</body>

</html>