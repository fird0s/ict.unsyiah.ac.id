<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>TIK Unsyiah</title>
    <!-- Bootstrap Styles-->
    <link href="<?php echo base_url('assets/admin/css/bootstrap.css') ?>" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="<?php echo base_url()?>assets/admin/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="<?php echo base_url()?>assets/admin/js/morris/morris-0.4.3.min.css" rel="stylesheet" />

    <!-- Custom Styles-->
    <link href="<?php echo base_url()?>assets/admin/css/custom-styles.css" rel="stylesheet" />
    <!-- Datepicker -->
    <link href="<?php echo base_url()?>assets/bootstrap-datepicker-1.5.1-dist/css/bootstrap-datepicker.min.css
" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/js/Lightweight-Chart/cssCharts.css"> 
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('admin/dashboard') ?>"><strong>Administrator TIK</strong></a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
               
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo site_url('admin/user_profile') ?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('admin/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
		<div id="sideNav" href=""><!-- <i class="fa fa-caret-right"></i> --></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">


                    <li>
                        <a href="<?php echo site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                    <li>
                        <a  href="#"><i class="fa fa-info"></i> Berita<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level ">
                            <li>
                                <a href="<?php echo site_url('admin/berita') ?>">Lihat Berita</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_berita') ?>">Tambah Berita</a>
                            </li>
                            
                            
                        </ul>
                    </li>


                     <li>
                        <a href="#"><i class="fa fa-calendar"></i> Event<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                           <li>
                                <a href="<?php echo site_url('admin/event') ?>">Lihat Event</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_event') ?>">Tambah Event</a>
                            </li>
                            
                            
                        </ul>
                    </li>



                    <li>
                        <a href="#"><i class="fa fa-file-text"></i> Pages<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin/page') ?>">Lihat Pages</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_page') ?>">Tambah Pages</a>
                            </li>
                            
                            
                        </ul>
                    </li>

                    <li>
                        <a  href="#"><i class="fa fa-quote-left"></i> Testimonial<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo site_url('admin/testimonial') ?>">Lihat Testimonial</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('admin/tambah_testimonial') ?>">Tambah Testimonial</a>
                            </li>
                            
                            
                        </ul>
                    </li>

                    <li class="active-menu">
                        <a href="<?php echo site_url('admin/settings') ?>"><i class="fa fa-desktop"></i> Pengaturan</a>
                    </li>
                    
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
      
		<div id="page-wrapper">
		  <div class="header"> 
                        <h1 class="page-header">
                            Admin Web Settings
                        </h1>
						<ol class="breadcrumb">
					  <li><a href="<?php echo site_url('admin/dashboard') ?>">Home</a></li>
                      <li class="active">Settings</li>
					</ol> 
									
		</div>
            <div id="page-inner">

                <!-- /. ROW  -->

                <div class="row">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Website Settings
                        </div>

                        <div class="panel-body">
                        <?php echo form_open_multipart(); ?>
                        <div class="col-md-10">


                        <form class="form-horizontal">
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Site Title</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value="<?php echo $settings ?>" placeholder="Site Title">
                            </div>
                          </div>
                          <br><br>

                          <div class="form-group">
                            <label class="col-sm-3 control-label">Site Description</label>
                            <div class="col-sm-9">
                              <textarea class="form-control" rows="2"></textarea> 
                            </div>
                          </div>
                          <br><br><br>

                          <div class="form-group">
                            <label class="col-sm-3 control-label">Tagline</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" placeholder="Slogan">
                            </div>
                          </div>

                          <br><br>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                              <input type="email" class="form-control" placeholder="Email">
                            </div>
                          </div>

                          <br><br>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Phone Number</label>
                            <div class="col-sm-9">
                              <input type="email" class="form-control" placeholder="Phone">
                            </div>
                          </div>

                          <br><br>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Office Address</label>
                            <div class="col-sm-9">
                              <input type="email" class="form-control" placeholder="Office Address">
                            </div>
                          </div>


                          <br><br>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Social Media</label>
                            <div class="col-sm-3">
                              <input type="email" class="form-control" placeholder="Facebook">
                            </div>
                            <div class="col-sm-2">
                              <input type="email" class="form-control" placeholder="Twitter">
                            </div>
                            <div class="col-sm-2">
                              <input type="email" class="form-control" placeholder="Youtube">
                            </div>
                            <div class="col-sm-2">
                              <input type="instagram" class="form-control" placeholder="Instagram">
                            </div>
                          </div>


                          <br><br>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Logo</label>
                            <div class="col-sm-9">
                              <input type="email" class="form-control" placeholder="Office Address">
                            </div>
                          </div>

                            <br>
                            <hr>
                            <button type="submit" name="submit" class="btn btn-default">Edit Settings</button>



                        </form>

                        <!-- </div> -->
                        

                            

                        </form>
                        </div>
                    </div>

                </div>
				
				
				
			
		
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="<?php echo base_url()?>assets/admin/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?php echo base_url()?>assets/admin/js/bootstrap.min.js"></script>
	 
    <!-- Metis Menu Js -->
    <script src="<?php echo base_url()?>assets/admin/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="<?php echo base_url()?>assets/admin/js/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/morris/morris.js"></script>
	
	
	<script src="<?php echo base_url()?>assets/admin/js/easypiechart.js"></script>
	<script src="<?php echo base_url()?>assets/admin/js/easypiechart-data.js"></script>
	
	 <script src="<?php echo base_url()?>assets/admin/js/Lightweight-Chart/jquery.chart.js"></script>
	
    <!-- Custom Js -->
    <script src="<?php echo base_url()?>assets/admin/js/custom-scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap-datepicker-1.5.1-dist/js/bootstrap-datepicker.min.js"></script>


      <script>
         CKEDITOR.replace( 'content',
         {
            filebrowserBrowseUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl : '<?php echo base_url()?>assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
        });
      </script>

</body>

</html>