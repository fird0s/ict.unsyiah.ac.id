<!DOCTYPE html>
<html>
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title>Sistem Informasi  - UPT TIK (Teknologi Informasi dan Komunikasi)</title>   

        <meta name="keywords" content="Sistem Informasi, Berita UPT TIK Unsyiah, UPT TIK UNSYIAH, Unsyiah, ICT Center Unsyiah" />
        <meta name="description" content="Sistem Informasi pada UPT TIK (Teknologi Informasi dan Komunikasi)">
        <meta name="author" content="UPT TIK, UNSYIAH, Universitas Syiah Kuala">

        <!-- Favicon -->
        <link href="<?php echo base_url('assets/img/favicon.png') ?>" rel="shortcut icon" type="image/png" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:500,400italic,700italic,300,100,700,500italic,400%7CMaterial+Icons+Extended%7COpen+Sans:300,400,600,700%7CProduct+Sans:400&lang=id" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css') ?>" >
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/simple-line-icons/css/simple-line-icons.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/owl.carousel/assets/owl.carousel.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/owl.carousel/assets/owl.theme.default.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/magnific-popup/magnific-popup.min.css') ?>">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-elements.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-blog.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-shop.css"') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-animate.css') ?>">

        <!-- Current Page CSS -->
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/settings.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/layers.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/navigation.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/circle-flip-slideshow/css/component.css') ?>" >

        <!-- Skin CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/default.css') ?>">

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>">

        <!-- Head Libs -->
        <script src="vendor/modernizr/modernizr.min.js"></script>

    </head>
    <body>

        <div class="body">
            <header id="header" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 57, "stickySetTop": "-57px", "stickyChangeLogo": true}'>
                <div class="header-body">
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-logo">
                                    <a href="<?php echo site_url('') ?>">
                                        <img alt="UPT TIK - UNSYIAH" width="315" height="80"  data-sticky-width="230" data-sticky-height="55" data-sticky-top="40" src="<?php echo base_url ('assets/img/logo.png') ?>">
                                    </a>
                                </div>
                            </div>
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-search hidden-xs">
                                        <form id="searchForm" action="page-search-results.html" method="get">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" id="q" placeholder="Search..." required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                    <nav class="header-nav-top hidden-xs">
                                        <ul class="nav nav-pills">
                                            <li class="hidden-xs">
                                                <a href="<?php echo site_url('contact') ?>"><i class="fa fa-angle-right"></i> Contact Us</a>
                                            </li>
                                            <li>
                                                <span class="ws-nowrap"><i class="fa fa-phone"></i> 0651-6303969</span>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="header-row">
                                    <div class="header-nav">
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                            <nav>
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <li class="dropdown">
                                                        <a class="" href="<?php echo site_url('') ?>">
                                                            Beranda
                                                        </a>
                                                    </li>

                                                    <li class="dropdown">
                                                        <a class="dropdown-toggle" href="#">
                                                            Profil UPT TIK
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?php echo site_url('page/detail/kepala-upt-tik') ?>">Kepala UPT TIK</a></li>
                                                            <li><a href="<?php echo site_url('page/detail/visi-dan-misi') ?>">Visi & Misi</a></li>
                                                            <li><a href="<?php echo site_url('/strukturorganisasi') ?>#semua">Struktur Organisasi</a></li>
                                                            <li><a href="<?php echo site_url('page/detail/sejarah') ?>">Sejarah</a></li>
                                                        </ul>
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('berita/') ?>">
                                                            Berita
                                                        </a>
                                                        
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('sisteminformasi') ?>">
                                                            Sistem Informasi
                                                        </a>
                                                    </li>

                                                    <li class="dropdown dropdown-mega">
                                                        <a href="<?php echo site_url('fasilitasdanlayanan') ?>#semua">
                                                            Fasilitas dan Layanan
                                                        </a>
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('contact') ?>">
                                                            Kontak Kami
                                                        </a>
                                                    </li>

                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div role="main" class="main">  

                <section class="page-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="<?php echo site_url('') ?>">Beranda</a></li>
                                    <li class="active">Sistem Informasi</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
                <div style="position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223); margin-top: -35px; margin-bottom: 30px;">
                            <img style="width: 100%; height=100%;>" src="<?php echo base_url ('assets/img/sisteminformasi.jpg') ?>">
                           </div> 

                <style type="text/css">
                
                </style>           
                

            </div>


            <!-- CONTENT -->

            <h3 style="text-align: center;">Sistem Sistem Informasi Univ. Syiah Kuala</h3><hr>
                            <div class="col-md-8 col-md-offset-2">
                                <p style="text-align: center;">Untuk meningkatkan kualitas Universitas Syiah Kuala, UPT TIK menciptakan Sistem Informasi untuk mendukung kegiatan civitas akademika agar efektif dan efisien.
 </p>
                            </div>


            <section class="section section-default section-with-mockup mb-none">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/KRSonline.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>KRS Online</strong></h3>
                                    <p>
                                        Aplikasi KRS Online merupakan suatu sistem informasi yang mengatur proses perkuliahan mahasiswa di civitas akademika unsyiah.
                                        Aplikasi KRS Online bertujuan untuk mempermudah mahasiswa untuk melaksanakan aktivitas akademik dimana data mahasiswa akan tersimpan dalam satu pusat data unsyiah.
                                    </p>
                                    <a target="_blank" href="http://krsonline.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                            </div>
                    </div>
                </section>

            <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Siakad</strong></h3>
                                    <p>
                                        Karena bertambahnya aplikasi-aplikasi yang kami kembangkan dan sediakan maka situs web kami yang lama bernama "Cyber-Campus" 
                                        kami perbaharuhi untuk memudahkan akses untuk Anda.
                                    </p>
                                    <a target="_blank" href="https://www.siakad.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/siakad.png') ?>">
                                </div>
                        </div>
                    </div>
                </section>   


             <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/doswal.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Dosen Wali</strong></h3>
                                    <p>
                                        Membantu antara mahasiswa dan dosen wali dapat saling berkomunikasi
                                        tanpa bertatap muka langsung. Tujuannya melakukan approval maupun penolakan terhadap mata kuliah yang telah diambil oleh mahasiswa
                                        . Dosen wali harus melakukan verifikasi untuk tiap tindakan yang telah diambil terhadap mata kuliah mahasiswa yang bersangkutan.
                                    </p>
                                     <a target="_blank" href="http://perwalian.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                        </div>
                    </div>
                </section>


            <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Pengusulan dan Pengelolaan Beasiswa</strong></h3>
                                    <p>
                                        Mengatur proses pengusulan dan pengelolaan beasiswa. Sistem ini berkomunikasi dengan SIAKAD untuk mengetahui status aktif dan IPK mahasiswa.
                                    </p>
                                    <a target="_blank" href="http://beasiswa.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/beasiswa.png') ?>">
                                </div>
                            </div>
                    </div>
                </section> 


              <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/simlit.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Manajemen Penelitian dan Pengabdian</strong></h3>
                                    <p>
                                        Gerbang Digital Kegiatan Penelitian dan Pengabdian. Kegiatan penelitian yang telah didanai sejak 2010 sebanyak 600+ judul.
                                    </p>
                                    <a target="_blank" href="http://simlit.unsyiah.ac.id/simlit3" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                        </div>
                    </div>
                </section>     



            <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Kepegawaian Universitas</strong></h3>
                                    <p>
                                        Aplikasi berbasis web yang digunakan untuk menunjang proses administrasi kepegawaian yang bertujuan untuk menciptakn informasi SDM selalu mutakhir.
                                    </p>
                                    <a target="_blank" href="http://simpeg.unsyiah.ac.id/simpeg-unsyiah/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/simpeg.png') ?>">
                                </div>
                            </div>
                    </div>
                </section>          


                 <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/repository.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Repository Publikasi Penelitian Universitas</strong></h3>
                                    <p>
                                        Aplikasi untuk melihat perkembangan, menemukan, atau menjadi kontributor data publikasi Unsyiah. Telah mencatat lebih dari 2874 publikasi.
                                    </p>
                                    <a target="_blank" href="http://www.rp2u.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                        </div>
                    </div>
                </section>  
            

            <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Survey Kepuasaan Dosen dan Tenaga Kependidikan</strong></h3>
                                    <p>
                                        Survei ini bertujuan untuk memperoleh masukan terhadap mutu pelayanan Unsyiah kepada Dosen dan Tenaga Kependidikan . Respon Bapak/Ibu sangat berharga dan akan digunakan oleh pimpinan Unsyiah untuk terus memperbaiki kualitas pelayanan birokrasi yang lebih baik. Survei berisi 14 pertanyaan untuk dosen dan 8 pertanyaan untuk tenaga kependidikan ditambah dengan saran dan memerlukan waktu kurang dari 10 menit untuk mengisinya. Mohon pertanyaan-pertanyaan dalam survei direspon berdasarkan pengetahuan atau pendapat bapak/ibu yang terbaik .
                                    </p>
                                    <a target="_blank" href="http://www.sikadik.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/survey.png') ?>">
                                </div>
                            </div>
                    </div>
                </section>  


                 <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/fsd.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Dashboard Dosen dan Staf (FSD)</strong></h3>
                                    <p>
                                        Semua sivitas akademik Unsyiah melalui apalikasi ini dapat memiliki website pribadi dan mendapatkan email @unsyiah.ac.id dengan mudah.
                                    </p>
                                    <a target="_blank" href="https://fsd.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                        </div>
                    </div>
                </section>  


                 <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Kuliah Kerja Nyata (KKN)</strong></h3>
                                    <p>
                                      Sistem informasi registrasi online Kuliah Kerja Nyata. Membantu mahasiswa dan penyelenggara KKN mengelola lebih cepat, akurat dan transparan.
                                    </p>
                                    <a target="_blank" href="http://kkn.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/kkn.png') ?>">
                                </div>
                            </div>
                    </div>
                </section>  

                 <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/sipkd.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Pengevaluasian Kinerja Dosen (SIPKD)</strong></h3>
                                    <p>
                                       Layanan untuk membantu mengevaluasi dan melaporkan akuntabilitas kinerja dosen kepada para pemangku kepentingan.
                                    </p>
                                    <a target="_blank" href="http://sipkd.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                        </div>
                    </div>
                </section>  


                 <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Pengaduan Layanan (SIPL)</strong></h3>
                                    <p>
                                      Sampaikan kritik dan saran kepada unit/pihak tertentu secara konstruktif demi peningkatan layanan Unsyiah.
                                    </p>
                                    <a target="_blank" href="https://pengaduan.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/sipl.png') ?>">
                                </div>
                            </div>
                    </div>
                </section>  


                 <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/simpkm.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Lembaga Pengabdian Kepada Masyarakat(SIMPKM)</strong></h3>
                                    <p>
                                       Pengusul proposal pengabdian kepada mayarakat dalam mengajukan proposal dan juga melihat hasil kelulusan.
                                    </p>
                                    <a target="_blank" href="http://lpkm.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                        </div>
                    </div>
                </section> 

                 <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Pre-Registrasi UKTB</strong></h3>
                                    <p>
                                      Aplikasi untuk mengisi Biodata Pre-Registrasi dan UKTB (Uang Kuliah Tunggal Berkeadilan) secara online (bisa diakses dari luat kampus).
                                    </p>
                                    <a target="_blank" href="https://uktb.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/uktb.png') ?>">
                                </div>
                            </div>
                    </div>
                </section>  


                <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/plo.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Paperless Office (PLO)</strong></h3>
                                    <p>
                                       Sebuah media online yang dimanfaatkan oleh segenap sivitas Unsyiah untuk berinteraksi dan berekspresi baik secara individu, unit maupun ke semua sivitas.
                                    </p>
                                    <a target="_blank" href="https://arsip.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                        </div>
                    </div>
                </section> 


                 <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Keuangan PNBP</strong></h3>
                                    <p>
                                     Simkeu PNBP adalah sistem informasi pengelolaan dana PNBP. Bertujuan agar proses pengusulan & penggunaan dana tercatat secara rapi dan transparan.
                                    </p>
                                    <a target="_blank" href="http://www.simkeu.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/simkeu.png') ?>">
                                </div>
                            </div>
                    </div>
                </section>  


                <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/tracer-study.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Tracer Study dan Alumni</strong></h3>
                                    <p>
                                      Sitalum adalah sistem informasi tracer study dan alumni. Melalui Sitalum, data alumni dikelola dengan baik dan hubungan antara almamater dan lulusan tetap terjalin.
                                    </p>
                                    <a target="_blank" href="http://cdc.unsyiah.ac.id/tracer-study" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                        </div>
                    </div>
                </section> 


                <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Kinerja dan Evaluasi Dosen (Sinekad)</strong></h3>
                                    <p>
                                     Evaluasi dan kinerja dosen diperoleh dan dinilai melalui kuisioner yang diisi oleh mahasiswa yang mengikuti mata kuliah.
                                    </p>
                                    <a target="_blank" href="http://sinekad.unsyiah.ac.id/limesurvey/index.php/716255?newtest=Y" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/sinekad.png') ?>">
                                </div>
                            </div>
                    </div>
                </section>


                <section class="section section-default">
                    <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/d3-register.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Pendaftaran Program D3 Online</strong></h3>
                                    <p>
                                      Aplikasi online untuk melakukan pendaftaran menjadi mahasiswa Unsyiah yang untuk jenjang Diploma 3 (D3)
                                    </p>
                                    <a target="_blank" href="http://www.pendaftarand3.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                        </div>
                    </div>
                </section>  


                <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Pendaftaran Alih Program Online</strong></h3>
                                    <p>
                                     Mulai tahun 2014, registrasi alih program dilakukan secara online, sementara ujian dan seleksi dilakukan secara lokal di fakultas terkait. 
                                    </p>
                                    <a target="_blank" href="http://www.alihprogram.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/alih-program.png') ?>">
                                </div>
                            </div>
                    </div>
                </section> 


                 <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/registrasi-pps.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Pendaftaran PPs Online</strong></h3>
                                    <p>
                                    Aplikasi online untuk melakukan pendaftaran menjadi mahasiswa Unsyiah yang untuk jenjang Pascasarjana (PPs).
                                    </p>
                                    <a target="_blank" href="http://www.pendaftaranpps.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                            </div>
                    </div>
                </section> 


                <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Data Statistik</strong></h3>
                                    <p>
                                    Data direpresentasikan dalam bentuk chart atau grafik. Aplikasi ini bersifat publik yang mana departemen terkait atau publik dapat memantau perkembangan.
                                    </p>
                                    <a target="_blank" href="http://data.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/data.png') ?>">
                                </div>
                            </div>
                    </div>
                </section> 


                <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/pmb.png') ?>">
                                </div>
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Laman Informasi Penerimaan Mahasiswa Baru</strong></h3>
                                    <p>
                                   Universitas Syiah Kuala menerima mahasiswa baru program sarjana untuk 59 program studi S1 melalui jalur SNMPTN dan SBMPTN.
                                    </p>
                                    <a target="_blank" href="http://pmb.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                            </div>
                    </div>
                </section> 


                <section class="section section-default">
                    <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="mt-xl">Sistem Informasi <strong>Akun Email Mahasiswa</strong></h3>
                                    <p>
                                   Aplikasi SIMAIL membantu mahasiswa untuk memperoleh akun email Unsyiah secara cepat dan lebih dekat tanpa harus datang langsung ke UPT TIK.
                                    </p>
                                    <a target="_blank" href="http://simail.unsyiah.ac.id/" class="btn btn-borders btn-quaternary mr-xs mb-sm">Kunjungi Halaman</a>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-thumbnail" style="width: 570px;" src="<?php echo base_url('assets/img/sistem-informasi/student-mail.png') ?>">
                                </div>
                            </div>
                    </div>
                </section> 


            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="footer-ribbon">
                            <span>Get in Touch</span>
                        </div>
                        <div class="col-md-3">
                            <div class="newsletter">
                                <h4 class="footer-underline">Tentang Kami</h4>
                                <ul class="tentang-kami">
                                    <li><a href="<?php echo site_url('page/detail/kepala-upt-tik') ?>">Kepala UPT TIK</a></li>
                                    <li><a href="<?php echo site_url('page/detail/visi-dan-misi') ?>">Visi & Misi</a></li>
                                    <li><a href="<?php echo site_url('/strukturorganisasi') ?>#semua">Struktur Organisasi</a></li>
                                    <li><a href="<?php echo site_url('page/detail/sejarah') ?>">Sejarah</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="contact-details">
                                <h4 class="footer-underline">Kantor Resmi</h4>
                                <ul class="contact">
                                    <li><p><i class="fa fa-map-marker"></i> <strong>Alamat:</strong> Gedung ICT Center Unsyiah - Taiwan <br>Jln. Teuku Nyak Arief Darussalam, Banda Aceh. </p></li>
                                    <li><p><i class="fa fa-phone"></i> <strong>HP:</strong> 0651-6303969</p></li>
                                    <li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:helpdesk.ict@unsyiah.ac.id">helpdesk.ict@unsyiah.ac.id</a></p></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <h4 class="footer-underline">Latest Tweets</h4>
                                <a class="twitter-timeline" height="70" data-theme="dark" data-tweet-limit="2" data-chrome="noscrollbar noheader nofooter transparent " href="https://twitter.com/ictunsyiah" data-widget-id="701963310241943552">Tweets by @ictunsyiah</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                        
                        <div class="col-md-2">
                            <h4 class="footer-underline">Follow Us</h4>
                            <ul class="social-icons">
                                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-icons-twitter"><a href="https://twitter.com/ictunsyiah" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1">
                                <a href="<?php echo site_url('') ?>" class="logo">
                                    <img alt="UPT TIK - UNSYIAH" class="img-responsive" src="<?php echo base_url ('assets/img/logo-white-font.png') ?>">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <p>© Copyright 2016. UPT TIK (Teknologi Informasi dan Komunikasi) Unsyiah</p>
                            </div>
                            <div class="col-md-4">
                                <nav id="sub-menu">
                                    <ul>
                                        <li><a href="page-faq.html">FAQ's</a></li>
                                        <li><a href="sitemap.html">Sitemap</a></li>
                                        <li><a href="contact-us.html">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <!-- Vendor -->
        <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.appear/jquery.appear.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.easing/jquery.easing.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery-cookie/jquery-cookie.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/common/common.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.validation/jquery.validation.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.stellar/jquery.stellar.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.gmap/jquery.gmap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.lazyload/jquery.lazyload.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/isotope/jquery.isotope.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/owl.carousel/owl.carousel.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/magnific-popup/jquery.magnific-popup.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/vide/vide.min.js') ?>"></script>
        
        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo base_url('assets/js/theme.js') ?>"></script>
        
        <!-- Current Page Vendor and Views -->
        <script src="<?php echo base_url('assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/views/view.home.js') ?>"></script>
        
        <!-- Theme Custom -->
        <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
        
        <!-- Theme Initialization Files -->
        <script src="<?php echo base_url('assets/js/theme.init.js') ?>"></script>

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->

        
    
    </body>
</html>
