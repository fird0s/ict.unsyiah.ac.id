<!DOCTYPE html>
<html>
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title><?php echo $page->page_name; ?> - UPT TIK (Teknologi Informasi dan Komunikasi)</title> 

        <meta name="keywords" content="<?php echo $page->page_name; ?>, Berita UPT TIK Unsyiah, UPT TIK UNSYIAH, Unsyiah, ICT Center Unsyiah" />
        <meta name="description" content="UPT TIK (Teknologi Informasi dan Komunikasi)">
        <meta name="author" content="UPT TIK, Unsyiah.">

        <!-- Favicon -->
        <link href="<?php echo base_url('assets/img/favicon.png') ?>" rel="shortcut icon" type="image/png" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:500,400italic,700italic,300,100,700,500italic,400%7CMaterial+Icons+Extended%7COpen+Sans:300,400,600,700%7CProduct+Sans:400&lang=id" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css') ?>" >
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/simple-line-icons/css/simple-line-icons.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/owl.carousel/assets/owl.carousel.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/owl.carousel/assets/owl.theme.default.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/magnific-popup/magnific-popup.min.css') ?>">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-elements.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-blog.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-shop.css"') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-animate.css') ?>">

        <!-- Current Page CSS -->
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/settings.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/layers.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/navigation.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/circle-flip-slideshow/css/component.css') ?>" >

        <!-- Skin CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/default.css') ?>">

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>">
 
        <!-- Head Libs -->
        <script src="<?php echo base_url('assets/vendor/modernizr/modernizr.min.js') ?>"></script>

    </head>
    <body>
        <div class="body">
            <header id="header" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 57, "stickySetTop": "-57px", "stickyChangeLogo": true}'>
                <div class="header-body">
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-logo">
                                    <a href="<?php echo site_url('') ?>">
                                        <img alt="UPT TIK - UNSYIAH" width="315" height="80"  data-sticky-width="230" data-sticky-height="55" data-sticky-top="40" src="<?php echo base_url ('assets/img/logo.png') ?>">
                                    </a>
                                </div>
                            </div>
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-search hidden-xs">
                                        <form id="searchForm" action="page-search-results.html" method="get">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" id="q" placeholder="Search..." required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                    <nav class="header-nav-top hidden-xs">
                                        <ul class="nav nav-pills">
                                            <li class="hidden-xs">
                                                <a href="<?php echo site_url('contact') ?>"><i class="fa fa-angle-right"></i> Contact Us</a>
                                            </li>
                                            <li>
                                                <span class="ws-nowrap"><i class="fa fa-phone"></i> 0651-6303969</span>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="header-row">
                                    <div class="header-nav">
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                        <!-- <ul class="header-social-icons social-icons hidden-xs">
                                            <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                            <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                            <li class="social-icons-youtube"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-youtube"></i></a></li>
                                        </ul> -->
                                        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                            <nav>
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <li class="dropdown">
                                                        <a class="" href="<?php echo site_url('') ?>">
                                                            Beranda
                                                        </a>
                                                    </li>

                                                    <li class="dropdown">
                                                        <a class="dropdown-toggle" href="#">
                                                            Profil UPT TIK
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?php echo site_url('page/detail/kepala-upt-tik') ?>">Kepala UPT TIK</a></li>
                                                            <li><a href="<?php echo site_url('page/detail/visi-dan-misi') ?>">Visi & Misi</a></li>
                                                            <li><a href="<?php echo site_url('/strukturorganisasi') ?>#semua">Struktur Organisasi</a></li>
                                                            <li><a href="<?php echo site_url('page/detail/sejarah') ?>">Sejarah</a></li>
                                                        </ul>
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('berita/') ?>">
                                                            Berita
                                                        </a>
                                                        
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('sisteminformasi') ?>">
                                                            Sistem Informasi
                                                        </a>
                                                    </li>

                                                    <li class="dropdown dropdown-mega">
                                                        <a href="<?php echo site_url('fasilitasdanlayanan') ?>#semua">
                                                            Fasilitas dan Layanan
                                                        </a>
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('contact') ?>">
                                                            Kontak Kami
                                                        </a>
                                                    </li>

                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            
            <div role="main" class="main">

                <section class="page-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="<?php echo site_url('') ?>">Beranda</a></li>
                                    <li><?php echo $page->page_name; ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container">

                    <div class="row">
                         <div class="col-md-4">
                            <aside class="sidebar">
                            
                                <h4 class="heading-primary">Profil UPT TIK</h4>

                                <ul class="nav nav-list mb-xlg">
                                    <li><a href="<?php echo site_url('page/detail/kepala-upt-tik') ?>">Kepala UPT TIK</a></li>
                                    <li><a href="<?php echo site_url('page/detail/visi-dan-misi') ?>">Visi dan Misi</a></li>
                                    <li><a href="<?php echo site_url('/strukturorganisasi') ?>#semua">Struktur Organisasi</a></li>
                                    <li><a href="<?php echo site_url('page/detail/sejarah') ?>">Sejarah</a></li>
                                </ul>

                                  <div class="tabs mb-xlg">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Berita Popular</a></li>
                                        <li><a href="#recentPosts" data-toggle="tab">Terbaru</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="popularPosts">
                                            <ul class="simple-post-list">
                                                <?php foreach ($popular->result_array() as $popular) { ?> 
                                                <li>
                                                    <div class="post-info">
                                                        <a href="<?php echo site_url('berita/detail') ?>/<?php echo $popular['slug'] ?>"><?php echo $popular['title'] ?></a>
                                                        <div class="post-meta">
                                                             <?php echo date('d F Y', strtotime( $popular['date_created'] )) ?>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="recentPosts">
                                            <ul class="simple-post-list">

                                               <?php foreach ($terbaru->result_array() as $recent) { ?> 
                                                <li>
                                                    <div class="post-info">
                                                        <a href="<?php echo site_url('berita/detail') ?>/<?php echo $recent['slug'] ?>"><?php echo $recent['title'] ?></a>
                                                        <div class="post-meta">
                                                             <?php echo date('d F Y', strtotime( $recent['date_created'] )) ?>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            
                            
                                <h4 class="heading-primary">Berita</h4>
                                <ul class="nav nav-list mb-xlg">
                                    <?php foreach ($kategori->result_array() as $kategori) { ?> 
                                    <li><a href="<?php echo site_url('berita/kategori')?>/<?php echo $kategori['slug'] ?>"><?php echo $kategori['category_name'] ?></a></li>
                                    <?php } ?>
                                </ul>
                            
                              
                            
                                <h4 class="heading-primary">Hubungi Kami</h4>
                                <ul class="list list-icons list-icons-style-3 mt-xlg">
                                    <li><p><i class="fa fa-map-marker"></i> <strong>Alamat:</strong> Gedung ICT Center Unsyiah - Taiwan <br>Jln. Teuku Nyak Arief Darussalam, Banda Aceh. </p></li>
                                    <li><p><i class="fa fa-phone"></i> <strong>HP:</strong> 0651-6303969</p></li>
                                    <li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:helpdesk.ict@unsyiah.ac.id">helpdesk.ict@unsyiah.ac.id</a></p></li>
                                </ul>
                            
                            </aside>
                        </div>

                        <div class="col-md-8">
                                    <div class="post-content">

                                        
                                        <div class="heading heading-border heading-bottom-border">
                                            <h2 class="heading-primary"><?php echo $page->page_name; ?></h2>
                                        </div>
                                        <?php echo html_entity_decode($page->content) ?>
                                        <div class="post-block post-share">
                                            <h3 class="heading-primary"><i class="fa fa-share"></i>Share this post</h3>

                                            <!-- AddThis Button BEGIN -->
                                            <div class="addthis_toolbox addthis_default_style ">
                                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                                <a class="addthis_button_tweet"></a>
                                                <a class="addthis_button_pinterest_pinit"></a>
                                                <a class="addthis_counter addthis_pill_style"></a>
                                            </div>
                                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
                                            <!-- AddThis Button END -->

                                        </div>

                                      
                                    </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
        
    <!-- Vendor -->
        <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.appear/jquery.appear.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.easing/jquery.easing.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery-cookie/jquery-cookie.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/common/common.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.validation/jquery.validation.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.stellar/jquery.stellar.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.gmap/jquery.gmap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.lazyload/jquery.lazyload.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/isotope/jquery.isotope.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/owl.carousel/owl.carousel.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/magnific-popup/jquery.magnific-popup.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/vide/vide.min.js') ?>"></script>
        
        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo base_url('assets/js/theme.js') ?>"></script>
        
        <!-- Current Page Vendor and Views -->
        <script src="<?php echo base_url('assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/views/view.home.js') ?>"></script>
        
        <!-- Theme Custom -->
        <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
        
        <!-- Theme Initialization Files -->
        <script src="<?php echo base_url('assets/js/theme.init.js') ?>"></script>

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->
        
    </body>
</html>
