<!DOCTYPE html>
<html>
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title>Struktur Organisasi - UPT TIK (Teknologi Informasi dan Komunikasi)</title>   

        <meta name="keywords" content="Fasilitas dan Layanan UPT TIK, Hubungi UPT TIK, Berita UPT TIK Unsyiah, UPT TIK UNSYIAH, Unsyiah, ICT Center Unsyiah" />
        <meta name="description" content="Struktur Organisasi UPT TIK (Teknologi Informasi dan Komunikasi)">
        <meta name="author" content="UPT TIK, UNSYIAH, Universitas Syiah Kuala">

        <!-- Favicon -->
        <link href="<?php echo base_url('assets/img/favicon.png') ?>" rel="shortcut icon" type="image/png" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:500,400italic,700italic,300,100,700,500italic,400%7CMaterial+Icons+Extended%7COpen+Sans:300,400,600,700%7CProduct+Sans:400&lang=id" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css') ?>" >
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/simple-line-icons/css/simple-line-icons.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/owl.carousel/assets/owl.carousel.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/owl.carousel/assets/owl.theme.default.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/vendor/magnific-popup/magnific-popup.min.css') ?>">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-elements.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-blog.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-shop.css"') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/theme-animate.css') ?>">

        <!-- Current Page CSS -->
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/settings.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/layers.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/rs-plugin/css/navigation.css') ?>" >
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/vendor/circle-flip-slideshow/css/component.css') ?>" >

        <!-- Skin CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/default.css') ?>">

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>">



        <!-- Head Libs -->
        <script src="vendor/modernizr/modernizr.min.js"></script>

        <style type="text/css">
        .thumb-info img {
            min-height: 200px;
        }

        .thumb-info-inner {
            font-size: 14px !important;
        }

        .thumb-info .thumb-info-title {
                padding: 1px 11px 9px;
        }
        .thumb-info .thumb-info-type {
            font-size: 13px;
        }
        </style>

    </head>
    <body>

        <div class="body">
            <header id="header" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 57, "stickySetTop": "-57px", "stickyChangeLogo": true}'>
                <div class="header-body">
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-logo">
                                    <a href="<?php echo site_url('') ?>">
                                        <img alt="UPT TIK - UNSYIAH" width="315" height="80"  data-sticky-width="230" data-sticky-height="55" data-sticky-top="40" src="<?php echo base_url ('assets/img/logo.png') ?>">
                                    </a>
                                </div>
                            </div>
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-search hidden-xs">
                                        <form id="searchForm" action="page-search-results.html" method="get">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="q" id="q" placeholder="Search..." required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                    <nav class="header-nav-top hidden-xs">
                                        <ul class="nav nav-pills">
                                            <li class="hidden-xs">
                                                <a href="<?php echo site_url('contact') ?>"><i class="fa fa-angle-right"></i> Contact Us</a>
                                            </li>
                                            <li>
                                                <span class="ws-nowrap"><i class="fa fa-phone"></i> 0651-6303969</span>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="header-row">
                                    <div class="header-nav">
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                            <nav>
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <li class="dropdown">
                                                        <a class="" href="<?php echo site_url('') ?>">
                                                            Beranda
                                                        </a>
                                                    </li>

                                                    <li class="dropdown">
                                                        <a class="dropdown-toggle" href="#">
                                                            Profil UPT TIK
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?php echo site_url('page/detail/kepala-upt-tik') ?>">Kepala UPT TIK</a></li>
                                                            <li><a href="<?php echo site_url('page/detail/visi-dan-misi') ?>">Visi & Misi</a></li>
                                                            <li><a href="<?php echo site_url('/strukturorganisasi') ?>#semua">Struktur Organisasi</a></li>
                                                            <li><a href="<?php echo site_url('page/detail/sejarah') ?>">Sejarah</a></li>
                                                        </ul>
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('berita/') ?>">
                                                            Berita
                                                        </a>
                                                        
                                                    </li>

                                                   <li class="">
                                                        <a class="" href="<?php echo site_url('sisteminformasi') ?>">
                                                            Sistem Informasi
                                                        </a>
                                                    </li>

                                                    <li class="dropdown">
                                                        <a href="<?php echo site_url('fasilitasdanlayanan') ?>#semua">
                                                            Fasilitas dan Layanan
                                                        </a>
                                                    </li>

                                                    <li class="">
                                                        <a class="" href="<?php echo site_url('contact') ?>">
                                                            Kontak Kami
                                                        </a>
                                                    </li>

                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div role="main" class="main">  

                <section class="page-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="breadcrumb">
                                    <li><a href="<?php echo site_url('') ?>">Beranda</a></li>
                                    <li class="active">Struktur Organisasi</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>


                <div class="container">

                    <h2>Meet the <strong>Team</strong></h2>

                    <ul class="nav nav-pills sort-source">
                        <li class="active"><a href="<?php echo site_url('/strukturorganisasi/') ?>">Show All</a></li>
                        <?php foreach ($devision->result_array() as $data) { ?>
                        <li><a href="<?php echo site_url('/strukturorganisasi/devision') ?>/<?php echo $data['slug'] ?>"><?php echo $data['devision'] ?></a></li>
                        <?php } ?>
                    </ul>

                    <hr>

                    <div class="row">

                        <ul class="team-list" data-sort-id="team" data-filter="*" style="position: relative; height: 881px;">
                            
                            <?php foreach ($employee->result_array() as $data) { ?>
                            <li class="col-md-3 col-sm-6 col-xs-12 isotope-item">
                                <span class="thumb-info thumb-info-hide-wrapper-bg mb-xlg">
                                    <span class="thumb-info-wrapper">
                                        <a target="_blank" href="<?php echo $data['website'] ?>">
                                            <img src="<?php echo base_url('./uploads/devision/')?>/<?php echo $data['photo_profile'] ?>" class="img-responsive" alt="">
                                            <span class="thumb-info-title">
                                                <!-- <span class="thumb-info-inner"><?php echo $data['name'] ?></span> -->
                                                <span class="thumb-info-type"><?php echo $data['name'] ?></span>
                                            </span>
                                        </a>
                                    </span>
                                </span>
                            </li>

                            <?php } ?>
                        </ul>

                    </div>

                </div>

            </div>


            <!-- CONTENT -->

            

            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="footer-ribbon">
                            <span>Get in Touch</span>
                        </div>
                        <div class="col-md-3">
                            <div class="newsletter">
                                <h4 class="footer-underline">Tentang Kami</h4>
                                <ul class="tentang-kami">
                                    <li><a href="<?php echo site_url('page/detail/kepala-upt-tik') ?>">Kepala UPT TIK</a></li>
                                    <li><a href="<?php echo site_url('page/detail/visi-dan-misi') ?>">Visi & Misi</a></li>
                                    <li><a href="<?php echo site_url('/strukturorganisasi') ?>#semua">Struktur Organisasi</a></li>
                                    <li><a href="<?php echo site_url('page/detail/sejarah') ?>">Sejarah</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="contact-details">
                                <h4 class="footer-underline">Kantor Resmi</h4>
                                <ul class="contact">
                                    <li><p><i class="fa fa-map-marker"></i> <strong>Alamat:</strong> Gedung ICT Center Unsyiah - Taiwan <br>Jln. Teuku Nyak Arief Darussalam, Banda Aceh. </p></li>
                                    <li><p><i class="fa fa-phone"></i> <strong>HP:</strong> 0651-6303969</p></li>
                                    <li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:helpdesk.ict@unsyiah.ac.id">helpdesk.ict@unsyiah.ac.id</a></p></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <h4 class="footer-underline">Latest Tweets</h4>
                                <a class="twitter-timeline" height="70" data-theme="dark" data-tweet-limit="2" data-chrome="noscrollbar noheader nofooter transparent " href="https://twitter.com/ictunsyiah" data-widget-id="701963310241943552">Tweets by @ictunsyiah</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                        
                        <div class="col-md-2">
                            <h4 class="footer-underline">Follow Us</h4>
                            <ul class="social-icons">
                                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-icons-twitter"><a href="https://twitter.com/ictunsyiah" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1">
                                <a href="<?php echo site_url('') ?>" class="logo">
                                    <img alt="UPT TIK - UNSYIAH" class="img-responsive" src="<?php echo base_url ('assets/img/logo-white-font.png') ?>">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <p>© Copyright 2016. UPT TIK (Teknologi Informasi dan Komunikasi) Unsyiah</p>
                            </div>
                            <div class="col-md-4">
                                <nav id="sub-menu">
                                    <ul>
                                        <li><a href="page-faq.html">FAQ's</a></li>
                                        <li><a href="sitemap.html">Sitemap</a></li>
                                        <li><a href="contact-us.html">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <!-- Vendor -->
        <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.appear/jquery.appear.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.easing/jquery.easing.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery-cookie/jquery-cookie.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/common/common.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.validation/jquery.validation.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.stellar/jquery.stellar.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.gmap/jquery.gmap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/jquery.lazyload/jquery.lazyload.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/isotope/jquery.isotope.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/owl.carousel/owl.carousel.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/magnific-popup/jquery.magnific-popup.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/vide/vide.min.js') ?>"></script>
        
        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo base_url('assets/js/theme.js') ?>"></script>
        
        <!-- Current Page Vendor and Views -->
        <script src="<?php echo base_url('assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/views/view.home.js') ?>"></script>
        
        <!-- Theme Custom -->
        <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
        
        <!-- Theme Initialization Files -->
        <script src="<?php echo base_url('assets/js/theme.init.js') ?>"></script>

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-12345678-1', 'auto');
            ga('send', 'pageview');
        </script>
         -->

        
    
    </body>
</html>
