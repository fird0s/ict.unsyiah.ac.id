###################
About Project
###################

The project is a website project that was developed by Muhammad Firdaus during his internship at [UPT TIK UNSYIAH](http://www.ict.unsyiah.ac.id/) from Feb-April 2016. The objective of the project is to increase brand value through website of UPT TIK UNSYIAH. This website will give information about **Information System and Infrastructure** that was developed by UPT TIK UNSYIAH. The existence of UPT TIK UNSYIAH has contributed greatly in UNSYIAH accreditation change from '**C**' to '**A**' that is because UPT TIK UNSYIAH has developed many Information System and Infrastructure to be more effective and efficient in order to improve the quality of Civitas Academica Unsyiah.
 
###################
About UPT. TIK (Information and Communication Technology) UNSYIAH
###################

UPT TIK is an [UNSYIAH University](http://www.unsyiah.ac.id/) unit that has an important position in technology development in accordance with the demands of Vision of Syiah Kuala University. Related to technology development, Unsyiah established UPT. ICT (Information and Communication Technology) to support the implementation of vision and mission of Syiah Kuala University.

###################
Web Technologies Used
###################

* PHP 7.0
* MySQL Database 
* Framework [Codeigniter 4.0.4](https://codeigniter.com/) 


###################
How to Deploy to Server
###################

1. Create folders "berita", "devision", "facilities_and_services", "partners", "slider", dan "testimonial" in "uploads" folder.
2. Change value $config['base_url'] in "application/config/config.php"


###################
Question?
###################

Email: firdauskoder@gmail.com